<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('users_companies', function (Blueprint $table) {
            $table->increments('company_id');
            $table->integer('user_id')->unsigned()->index();
            $table->text('logo');
            $table->text('coverbg');
            $table->enum('type', ['MB', 'UAB', 'AB', 'IV'])->index();
            $table->string('name');
            $table->string('description');
            $table->string('email');
            $table->string('mobilephone');
            $table->string('website');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('users_companies');
    }
}
