<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocale extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('slug');
        });
        Schema::table('subcategories', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('slug');
        });
        Schema::table('subsubcategories', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('slug');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('city_group');
        });
        Schema::table('microdistricts', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('microdistrict_name');
        });
        Schema::table('municipalitys', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('municipality_name');
        });
        Schema::table('places', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('place_name');
        });
        Schema::table('options', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('option_name');
        });
        Schema::table('types', function (Blueprint $table) {
            $table->string('locale')->default('lt')->after('type_name');
        });

        // CAR
         Schema::table('car_bodys', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_body_name');
         });
         Schema::table('car_climates', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_climate_name');
         });
         Schema::table('car_colors', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_color_name');
         });
         Schema::table('car_damageds', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_damage_name');
         });
         Schema::table('car_firstcountries', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_country_name');
         });
         Schema::table('car_fuels', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_fuel_name');
         });
         Schema::table('car_gearboxs', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_gearbox_name');
         });
         Schema::table('car_transmisions', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_transmision_name');
         });
         Schema::table('car_wheels', function (Blueprint $table) {
             $table->string('locale')->default('lt')->after('car_wheel_name');
         });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('subcategories', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('subsubcategories', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('microdistricts', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('municipalitys', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('places', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('options', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('types', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });

        // CAR
        Schema::table('car_bodys', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_climates', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_colors', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_damageds', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_firstcountries', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_fuels', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_gearboxs', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_transmisions', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
        Schema::table('car_wheels', function (Blueprint $table) {
            $table->dropColumn(['locale']);
        });
    }
}
