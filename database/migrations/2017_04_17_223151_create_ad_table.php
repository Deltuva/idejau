<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ad', function (Blueprint $table) {
            $table->increments('ad_id');
            $table->integer('user_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->integer('type_id')->unsigned()->index();
            $table->string('ad_title')->nullable();
            $table->string('ad_description')->nullable();
            $table->string('ad_description_hash', 40)->nullable();
            $table->boolean('ad_active')->default(false);
            $table->string('ad_ip', 20)->nullable();
            $table->double('ad_price', 10, 2);
            $table->boolean('ad_free')->default(false);
            $table->string('ad_link')->nullable();
            $table->string('ad_video')->nullable();
            $table->string('ad_lat_lng')->nullable();
            $table->string('ad_phone')->nullable();
            $table->boolean('ad_phone_hide')->default(false);
            $table->string('ad_email')->nullable();
            $table->string('ad_skype')->nullable();
            $table->string('ad_address')->nullable();
            $table->integer('ad_view');
            $table->integer('car_make_id')->unsigned()->nullable()->index();
            $table->integer('car_model_id')->unsigned()->nullable()->index();
            $table->integer('car_engine_id')->unsigned()->nullable()->index();
            $table->integer('car_body_id')->unsigned()->nullable()->index();
            $table->integer('car_climate_id')->unsigned()->nullable()->index();
            $table->integer('car_color_id')->unsigned()->nullable()->index();
            $table->integer('car_damage_id')->unsigned()->nullable()->index();
            $table->integer('car_door_id')->unsigned()->nullable()->index();
            $table->integer('car_country_id')->unsigned()->nullable()->index();
            $table->integer('car_fuel_id')->unsigned()->nullable()->index();
            $table->integer('car_consuption_id')->unsigned()->nullable()->index();
            $table->integer('car_gearbox_id')->unsigned()->nullable()->index();
            $table->integer('car_year_id')->unsigned()->nullable()->index();
            $table->integer('car_month_id')->unsigned()->nullable()->index();
            $table->integer('car_power_id')->unsigned()->nullable()->index();
            $table->integer('car_rida_id')->unsigned()->nullable()->index();
            $table->integer('car_transmision_id')->unsigned()->nullable()->index();
            $table->integer('car_wheel_id')->unsigned()->nullable()->index();
            $table->char('code', 30);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('location_id')->references('id')->on('cities');
            // $table->foreign('type_id')->references('type_id')->on('types');
            // $table->foreign('car_make_id')->references('car_make_id')->on('car_makes');
            // $table->foreign('car_model_id')->references('car_model_id')->on('car_models');
            // $table->foreign('car_engine_id')->references('car_engine_id')->on('car_engines');
            // $table->foreign('car_body_id')->references('car_body_id')->on('car_bodys');
            // $table->foreign('car_climate_id')->references('car_climate_id')->on('car_climates');
            // $table->foreign('car_color_id')->references('car_color_id')->on('car_colors');
            // $table->foreign('car_damage_id')->references('car_damage_id')->on('car_damageds');
            // $table->foreign('car_door_id')->references('car_door_id')->on('car_doors');
            // $table->foreign('car_country_id')->references('car_country_id')->on('car_firstcountries');
            // $table->foreign('car_fuel_id')->references('car_fuel_id')->on('car_fuels');
            // $table->foreign('car_consuption_id')->references('car_consuption_id')->on('car_fuelconsuptions');
            // $table->foreign('car_gearbox_id')->references('car_gearbox_id')->on('car_gearboxs');
            // $table->foreign('car_year_id')->references('car_year_id')->on('car_years');
            // $table->foreign('car_month_id')->references('car_month_id')->on('car_months');
            // $table->foreign('car_power_id')->references('car_power_id')->on('car_powers');
            // $table->foreign('car_rida_id')->references('car_rida_id')->on('car_ridas');
            // $table->foreign('car_transmision_id')->references('car_transmision_id')->on('car_transmisions');
            // $table->foreign('car_wheel_id')->references('car_wheel_id')->on('car_wheels');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ad');
    }
}
