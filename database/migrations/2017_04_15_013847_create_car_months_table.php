<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarMonthsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('car_months', function (Blueprint $table) {
            $table->increments('car_month_id');
            $table->string('car_month_name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('car_months');
    }
}
