<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('ad_categories', function (Blueprint $table) {
            $table->increments('ad_category_id');
            $table->integer('ad_id')->unsigned()->index();
            $table->integer('cat_id')->unsigned()->index();
            $table->integer('sub_id')->unsigned()->index();
            $table->integer('subsub_id')->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('ad_categories');
    }
}
