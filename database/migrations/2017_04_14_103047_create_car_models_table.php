<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarModelsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('car_models', function (Blueprint $table) {
            $table->increments('car_model_id');
            $table->integer('car_make_id')->unsigned()->index();
            $table->string('car_model_name');
            $table->string('car_model_year');
            $table->boolean('car_model_active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('car_models');
    }
}
