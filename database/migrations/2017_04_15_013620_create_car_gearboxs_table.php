<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarGearboxsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('car_gearboxs', function (Blueprint $table) {
            $table->increments('car_gearbox_id');
            $table->string('car_gearbox_name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('car_gearboxs');
    }
}
