<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarYearsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('car_years', function (Blueprint $table) {
            $table->increments('car_year_id');
            $table->string('car_year_name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('car_years');
    }
}
