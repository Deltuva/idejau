<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarEngineValueToCarEnginesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_engines', function (Blueprint $table) {
            $table->decimal('car_engine_value', 5, 1)->after('car_engine_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_engines', function (Blueprint $table) {
            $table->dropColumn(['car_engine_value']);
        });
    }
}
