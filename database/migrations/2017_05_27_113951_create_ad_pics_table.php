<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_pics', function (Blueprint $table) {
            $table->increments('adpic_id');
            $table->text('adpic_file');
            $table->integer('ad_id')->unsigned()->index();
            $table->boolean('adpic_show')->default(true);
            $table->string('adpic_event');
            $table->timestamps();

            $table->foreign('ad_id')->references('ad_id')->on('ad')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_pics');
    }
}
