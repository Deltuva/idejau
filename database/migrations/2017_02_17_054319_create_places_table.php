<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('place_id');
            $table->integer('municipality_id')->unsigned()->index();
            $table->string('place_name')->nullable();

            $table->foreign('municipality_id')->references('muni_id')->on('municipalitys')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->dropIndex(['municipality_id']);
        });
        Schema::drop('places');
    }
}
