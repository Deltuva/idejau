<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarRidaValueToCarRidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_ridas', function (Blueprint $table) {
            $table->integer('car_rida_value')->unsigned()->default(0)->index()->after('car_rida_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_ridas', function (Blueprint $table) {
            $table->dropColumn(['car_rida_value']);
        });
    }
}
