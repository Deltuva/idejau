<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarPowerValueToCarPowersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('car_powers', function (Blueprint $table) {
            $table->string('car_power_value')->after('car_power_name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('car_powers', function (Blueprint $table) {
            $table->dropColumn(['car_power_value']);
        });
    }
}
