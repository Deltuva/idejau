<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('subsubcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subcategory_id')->unsigned()->nulable()->comment('Subkategorija');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();

            $table->foreign('subcategory_id')->references('id')->on('subcategories');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('subparentcategories');
    }
}
