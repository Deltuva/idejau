<?php

use Illuminate\Database\Seeder;
use App\Municipality;

class MunicipalitysTableSeeder extends Seeder
{
    /**
      * Run the database seeds.
      */
     public function run()
     {
         $municipalitys = [
           'Vilniaus m. sav.',
           'Kauno m. sav.',
           'Klaipėdos m. sav.',
           'Šiaulių m. sav.',
           'Panevėžio m. sav.',
           'Alytaus m. sav.',
           'Marijampolės m. sav',
           'Utenos m. sav',
           'Palangos m. sav.',
           'Akmenės r. sav.',
           'Alytaus r. sav.',
           'Anykščių r. sav.',
           'Birštono sav.',
           'Biržų r. sav.',
           'Druskininkų sav.',
           'Elektrėnų sav.',
           'Ignalinos r. sav.',
           'Jonavos r. sav.',
           'Joniškio r. sav.',
           'Jurbarko r. sav.',
           'Kaišiadorių r. sav.',
           'Kalvarijos sav.',
           'Kauno r. sav.',
           'Kazlų Rūdos sav.',
           'Kėdainių r. sav.',
           'Kelmės r. sav.',
           'Klaipėdos r. sav.',
           'Kretingos r. sav.',
           'Kupiškio r. sav.',
           'Lazdijų r. sav.',
           'Marijampolės r. sav.',
           'Mažeikių r. sav.',
           'Molėtų r. sav.',
           'Neringos sav.',
           'Pagėgių sav.',
           'Pakruojo r. sav.',
           'Panevėžio r. sav.',
           'Pasvalio r. sav.',
           'Plungės r. sav.',
           'Prienų r. sav.',
           'Radviliškio r. sav.',
           'Raseinių r. sav.',
           'Rietavo sav.',
           'Rokiškio r. sav.',
           'Šakių r. sav.',
           'Šalčininkų r. sav.',
           'Šiaulių r. sav.',
           'Šilalės r. sav.',
           'Šilutės r. sav.',
           'Širvintų r. sav.',
           'Skuodo r. sav.',
           'Švenčionių r. sav.',
           'Tauragės r. sav.',
           'Telšių r. sav.',
           'Trakų r. sav.',
           'Ukmergės r. sav.',
           'Utenos r. sav.',
           'Varėnos r. sav.',
           'Vilkaviškio r. sav.',
           'Vilniaus r. sav.',
           'Visagino sav.',
           'Zarasų r. sav.',
           'Užsienis'
         ];

         foreach ($municipalitys as $municipality) {
             Municipality::create([
               'municipality_name' => $municipality,
             ]);
         }
     }
}
