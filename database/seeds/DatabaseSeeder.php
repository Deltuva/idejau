<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->command->info('Running UpdateSeeder...');

        // $this->call(CarMarksTableSeeder::class);
        // $this->command->info("CarMarks table seeded.");

        // $this->call(CarMarksModelsTableSeeder::class);
        // $this->command->info("CarMarksModels table seeded.");

        // $this->call(MunicipalitysTableSeeder::class);
        // $this->command->info("Municipalitys table seeded.");

        // $this->call(PlacesTableSeeder::class);
        // $this->command->info('Places table seeded.');

        // $this->call(MicrodistrictsTableSeeder::class);
        // $this->command->info('Places Microdistricts table seeded.');

        // $this->call(CarYearsTableSeeder::class);
        // $this->command->info('Car Years table seeded.');
    }
}
