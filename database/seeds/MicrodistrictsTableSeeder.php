<?php

use Illuminate\Database\Seeder;
use App\Microdistrict;

class MicrodistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $microdistrict_vilnius = [
          'Antakalnis',
          'Antežeriai',
          'Aukštieji Paneriai',
          'Avižieniai',
          'Bajorai',
          'Balsiai',
          'Baltupiai',
          'Bendorėliai',
          'Buivydiškės',
          'Bukčiai',
          'Burbiškės',
          'Didieji Gulbinai',
          'Didžioji Riešė',
          'Dvarčionys',
          'Fabijoniškės',
          'Filaretai',
          'Gariūnai',
          'Gineitiškės',
          'Grigiškės',
          'Gudeliai',
          'Jeruzalė',
          'Justiniškės',
          'Kairėnai',
          'Kalnėnai',
          'Karoliniškės',
          'Kirtimai',
          'Klevinė',
          'Lazdynai',
          'Lazdynėliai',
          'Liepkalnis',
          'Markučiai',
          'Mažieji Gulbinai',
          'Mažoji Riešė',
          'Naujamiestis',
          'Naujaneriai',
          'Naujieji Verkiai',
          'Naujininkai',
          'Naujoji Vilnia',
          'Pagiriai',
          'Paneriai',
          'Pašilaičiai',
          'Pavilnys',
          'Pilaitė',
          'Rasos',
          'Salininkai',
          'Santariškės',
          'Saulėtekis',
          'Senamiestis',
          'Šeškinė',
          'Šiaurės miestelis',
          'Šilas',
          'Skaidiškės',
          'Šnipiškės',
          'Stoties',
          'Tarandė',
          'Trakų Vokė',
          'Užupis',
          'Valakampiai',
          'Verkiai',
          'Vilkpėdė',
          'Viršuliškės',
          'Visoriai',
          'Žemieji Paneriai',
          'Žirmūnai',
          'Zujūnai',
          'Žvėrynas',
          'Turniškės',
        ];

        $microdistrict_kaunas = [
          'Aleksotas',
          'Aukštieji Šančiai',
          'Centras',
          'Dainava',
          'Eiguliai',
          'Freda',
          'Kalniečiai',
          'Kaniūkai',
          'Kleboniškis',
          'Lampėdžiai',
          'Naujamiestis',
          'Noreikiškės',
          'Palemonas',
          'Panemunė',
          'Petrašiūnai',
          'Reportas',
          'Romainiai',
          'Sargėnai',
          'Senamiestis',
          'Šilainiai',
          'Vaišvydava',
          'Vičiūnai',
          'Vilijampolė',
          'Vytėnai',
          'Žaliakalnis',
          'Žemieji Šančiai',
        ];

        $microdistrict_klaipeda = [
          'Alksnynė',
          'Baltijos',
          'Bandužiai',
          'Centras',
          'Gedminai',
          'Ginduliai',
          'Giruliai',
          'Jakai',
          'Kauno',
          'Labrenciškės',
          'Mažojo kaimelio',
          'Melnragė',
          'Miško',
          'Mokyklos',
          'Naujakiemis',
          'Paupiai',
          'Rimkai',
          'Senamiestis',
          'Smeltė',
          'Sudmantai',
          'Tauralaukis',
          'Trinyčiai',
          'Vėtrungė',
          'Vingio',
          'Vitė',
          'Žvejybos uostas',
        ];

        $microdistrict_siauliai = [
          'Aleksandrija',
          'Bačiūnai',
          'Centras',
          'Dainiai',
          'Ginkūnai',
          'Gytarai',
          'Gubernija',
          'Lieporiai',
          'Medelynas',
          'Pabaliai',
          'Rėkyva',
          'Šimšė',
          'Verduliai',
          'Žaliūkiai',
          'Zokniai',
        ];

        $microdistrict_panevezys = [
          'Centras',
          'Kniaudiškis',
          'Molainiai',
          'Pilėnai',
          'Rožės',
          'Skaistakalnis',
          'Stetiškiai',
          'Tulpės',
          'Vaivados',
          'Žemaičiai',
          'Pramonės',
          'Klaipėdos',
        ];

        $microdistrict_panevezys = [
          'Centras',
          'Kniaudiškis',
          'Molainiai',
          'Pilėnai',
          'Rožės',
          'Skaistakalnis',
          'Stetiškiai',
          'Tulpės',
          'Vaivados',
          'Žemaičiai',
          'Pramonės',
          'Klaipėdos',
        ];

        $microdistrict_alytus = [
          'Dainava',
          'Likiškėlių',
          'Likiškių',
          'Pirmas Alytus',
          'Putinų',
          'Senamiestis',
          'Vidzgiris',
        ];

        $microdistrict_marijampole = [
          'Centras',
          'Degučiai',
          'Laikštė',
          'Mokolai',
          'Mokolų kaimas',
          'Stachonoviečių',
          'Tarpučiai',
        ];

        $microdistrict_utena = [
          'Aukštakalnio',
          'Ąžuolijos',
          'Dauniškio',
          'Grybelių',
          'Krašuonos',
          'Senamiestis',
          'Smėlio',
          'Vyturių',
        ];

        $microdistrict_palanga = [
          'Palangos m.',
          'Šventoji',
        ];

        foreach ($microdistrict_vilnius as $microdistrict) {
            Microdistrict::create([
            'place_id' => 1,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_kaunas as $microdistrict) {
            Microdistrict::create([
            'place_id' => 81,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_klaipeda as $microdistrict) {
            Microdistrict::create([
            'place_id' => 82,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_siauliai as $microdistrict) {
            Microdistrict::create([
            'place_id' => 83,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_panevezys as $microdistrict) {
            Microdistrict::create([
            'place_id' => 84,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_alytus as $microdistrict) {
            Microdistrict::create([
            'place_id' => 85,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_marijampole as $microdistrict) {
            Microdistrict::create([
            'place_id' => 86,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_marijampole as $microdistrict) {
            Microdistrict::create([
            'place_id' => 86,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_utena as $microdistrict) {
            Microdistrict::create([
            'place_id' => 87,
            'microdistrict_name' => $microdistrict,
          ]);
        }

        foreach ($microdistrict_palanga as $microdistrict) {
            Microdistrict::create([
            'place_id' => 88,
            'microdistrict_name' => $microdistrict,
          ]);
        }
    }
}
