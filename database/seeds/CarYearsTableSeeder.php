<?php

use Illuminate\Database\Seeder;
use App\CarYear;

class CarYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        foreach (array_combine(range(date('Y'), 1925), range(date('Y'), 1925)) as $year) {
            CarYear::create([
            'car_year_name' => $year,
          ]);
        }
    }
}
