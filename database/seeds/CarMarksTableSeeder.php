<?php

use Illuminate\Database\Seeder;

class CarMarksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $marks = [
          'Acura',
          'Alfa Romeo',
          'Amc',
          'Aston Martin',
          'Audi',
          'Avanti',
          'Bentley',
          'BMW',
          'Buick',
          'Cadillac',
          'Chevrolet',
          'Chrysler',
          'Daewoo',
          'Daihatsu',
          'Datsun',
          'Delorean',
          'Dodge',
          'Eagle',
          'Ferrari',
          'Fiat',
          'Fisker',
          'Ford',
          'Freightliner',
          'Geo',
          'Gmc',
          'Honda',
          'Hummer',
          'Hyundai',
          'Infiniti',
          'Isuzu',
          'Jaguar',
          'Jeep',
          'Kia',
          'Lamborghini',
          'Lancia',
          'Land Rover',
          'Lexus',
          'Lincoln',
          'Lotus',
          'Maserati',
          'Maybach',
          'Mazda',
          'McLaren',
          'Mercedes Benz',
          'Mercury',
          'Merkur',
          'Mini',
          'Mitsubishi',
          'Nissan',
          'Oldsmobile',
          'Peugeot',
          'Plymouth',
          'Pontiac',
          'Porshe',
          'Ram',
          'Renault',
          'Rolls Royce',
          'SABB',
          'Saturn',
          'Scion',
          'Smart',
          'Srt',
          'Sterling',
          'Subaru',
          'Suzuki',
          'Tesla',
          'Toyota',
          'Triumph',
          'Volkswagen',
          'Volvo',
          'Yugo',
          'Aptera',
          'Bugatti',
          'Morgan',
          'Panoz',
          'Spyker',
          'Citroen',
          'Shelby',
          'Ford',
          'Kita',
        ];

        foreach ($marks as $mark) {
            CarMake::create([
              'car_make_name' => $mark,
            ]);
        }
    }
}
