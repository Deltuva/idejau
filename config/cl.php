<?php

return [
    'site_title' => 'Skelbimai',
    'site_domain' => 'idejau.lt',
    'enable_categories' => true,
    'enable_latest_posts' => true,
    'enable_magic_keywords' => true,
    'enable_recaptcha' => false,
    'cache_expire' => 0,
];
