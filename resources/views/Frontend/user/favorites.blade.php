@extends('frontend/layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h1 class="login-title">
          {{ trans('home.navbar.me_favorites_posts') }}
        </h1>
        <div class="cl-ad-block">
          @if ($userFavorites->count() > 0)
            @foreach($userFavorites as $favorite)
              <div class="cl-ad__item">
                <div class="row">
                  <div class="col-xs-4 col-sm-2 ad__image">
                    <a target="_blank" href="#">
                      <img src="{{ $favorite->getImage() }}" alt="{{ trans('home.ad.thumb_loading') }}" class="img-responsive">
                    </a>
                    <div class="vt-pic">
                      {!! $favorite->getImageCount() !!}
                    </div>
                    {{-- @if ($favorite->ad>getLatestVisitStatus())
                      <div class="vt-bt">
                        {{ trans('home.ad.latest_visited_status') }}
                      </div>
                    @endif --}}
                  </div>
                  <div class="col-xs-8 col-sm-10 ad__content">
                    <h3><a target="_blank" href="#">
                    {!! $favorite->getTitleName() !!}</a></h3>
                    <div class="ad__price clearfix">
                      <span class="price">
                        {{ $favorite->getFormatedPrice() }}
                      </span>
                      {!! $favorite->getStarsCountWidget() !!}
                      {!! $favorite->getTodayWidget() !!}
                    </div>
                    <div class="description">
                      {{ $favorite->getLimitDescription() }}
                      <div class="hidden-xs">
                        <p>{!! $favorite->getMoreDescription() !!}</p>
                      </div>
                    </div>
                    <div class="location hidden-xs">
                      {{ $favorite->getLocationName() }}
                    </div>
                    <div class="more-f hidden-xs clearfix">
                      <dm-favorite :favorite="{{ $favorite->ad_id }}" 
                         :isfavorited="{{ $favorite->isFavorited() }}">
                      </dm-favorite>
                      {!! $favorite->getCompanyDetail() !!}
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
                @if ($userFavorites->lastPage() > 0)
                    {!! $userFavorites->appends(request()->query())
                        ->render() !!}
                @endif
              @else 
                <div class="alert alert-danger text-center" style="margin-top: 5px;">
                  {{ trans('home.ad.no_favorite_ads') }}.
                </div>
            @endif
        </div>
      </div>
    </div>
  </div>
@endsection
