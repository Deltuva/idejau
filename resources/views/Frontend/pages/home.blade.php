@extends('frontend/layouts.app')

@section('content')
  {{-- <example></example> --}}
  @if(config('cl.enable_categories') && !$categories->isEmpty())
    <div class="container">
      {{-- <div>
        <p v-cloak>@{{ message }}</p>
        <button v-on:click="reverseMessage">Reverse Message</button>
      </div> --}}
      <div class="row">
        <div class="col-xs-12">
          <div class="row">
            @if(isset($categories) && !empty($categories))

              @foreach($categories as $category)
                <div class="col-xs-12 col-sm-6 col-md-3">
                  <div class="cl-category-block">

                    @if(!is_null($category->name))
                      <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-4">
                          <a href="{{ url('skelbimai',$category->categorySlugAt) }}">
                            @if(!empty($category->icon))
                              <img src="{{ asset('images/icons/'.$category->icon.'.png') }}" alt="{{ $category->name }}">
                            @endif
                          </a>
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-8 no-padding-left">
                          <h2><a href="{{ url('skelbimai',$category->categorySlugAt) }}">{{ $category->name }}</a></h2>
                        </div>
                      </div>
                    @endif

                    @if($category->subcategories->count() > 0)
                      <ul>
                        @foreach($category->subcategories()->with('listingSubcategories')->get() as $subcategory)
                          @if(!is_null($subcategory->name))
                            <li>
                              <a href="{{ url("skelbimai",$subcategory->subcategorySlugAt) }}"  >{{ $subcategory->categoryNameReplaceAt }}
                                <span class="count-badge">({{ $subcategory->countOfSubCategoriesAt }})</span></a>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                    @endif
                  </div>
                </div>
              @endforeach
            @endif
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection
