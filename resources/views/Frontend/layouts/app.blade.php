<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <!--        $$\           $$\   $$\                                    -->
    <!--        $$ |          $$ |  $$ |                                   -->
    <!--   $$$$$$$ | $$$$$$\  $$ |$$$$$$\   $$\   $$\ $$\    $$\ $$$$$$\   -->
    <!--  $$  __$$ |$$  __$$\ $$ |\_$$  _|  $$ |  $$ |\$$\  $$  |\____$$\  -->
    <!--  $$ /  $$ |$$$$$$$$ |$$ |  $$ |    $$ |  $$ | \$$\$$  / $$$$$$$ | -->
    <!--  $$ |  $$ |$$   ____|$$ |  $$ |$$\ $$ |  $$ |  \$$$  / $$  __$$ | -->
    <!--  \$$$$$$$ |\$$$$$$$\ $$ |  \$$$$  |\$$$$$$  |   \$  /  \$$$$$$$ | -->
    <!--   \_______| \_______|\__|   \____/  \______/     \_/    \_______| -->
    <!-- Fonts -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    {{-- <link href='//fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600,700&amp;subset=latin,latin-ext'
                rel='stylesheet' type='text/css'> --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Titillium+Web&subset=latin,latin-ext" rel="stylesheet"> --}}
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
    <script>
        window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
  <div id="app">
    @include('frontend/partials.navbar')
    @yield('breadcrumb')
    @yield('content')
  </div>
    <footer class="s-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p>&copy; 2017 Idejau.lt - by Interneto sprendimų grupė</p>
                </div>
            </div>
        </div>
    </footer>

<script>
    __dm_car_make = '{{ trans('home.filter_data.car_make') }}';
    __dm_car_model = '{{ trans('home.filter_data.car_model') }}';
    __dm_car_power = '{{ trans('home.filter_data.car_power') }}';
    __dm_car_fuel = '{{ trans('home.filter_data.car_fuel_type') }}';
    __dm_car_gearbox = '{{ trans('home.filter_data.car_gearbox') }}';
    __dm_car_km = '{{ trans('home.filter_data.car_rida') }}';
    __dm_car_body = '{{ trans('home.filter_data.car_body_type') }}';
    __dm_car_wheel = '{{ trans('home.filter_data.car_wheel_position') }}';
    __dm_car_year = '{{ trans('home.filter_data.car_year') }}';
    __dm_car_engine = '{{ trans('home.filter_data.car_engine_capacity') }}';
    __dm_car_color = '{{ trans('home.filter_data.car_color') }}';
    __dm_car_damage = '{{ trans('home.filter_data.car_has_damaged') }}';
    __dm_not_btn = '{{ trans('home.filter_data.not_btn') }}';
    __dm_empty_data = '{{ trans('home.filter_data.empty_data') }}';
    __dm_loading = '{{ trans('home.filter_data.loading') }}';
    __dm_filter_q_placeholder = '{{ trans('home.filter_data.filter_q_placeholder') }}';
    __dm_filter_q_found = '{{ trans('home.filter_data.filter_q_found') }}';
    __dm_select_all = '{{ trans('home.filter_data.select_all') }}';
    __dm_select_all_clear = '{{ trans('home.filter_data.select_all_clear') }}';
    __dm_all_price = '{{ trans('home.filter_data.all_price') }}';
    __dm_all_price_gte = '{{ trans('home.filter_data.all_price_gte') }}';
    __dm_all_price_lte = '{{ trans('home.filter_data.all_price_lte') }}';
    __dm_add_to_favorite = '{{ trans('home.ad.remember') }}';
    __dm_remove_from_favorite = '{{ trans('home.ad.unremember') }}';
</script>

<script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
