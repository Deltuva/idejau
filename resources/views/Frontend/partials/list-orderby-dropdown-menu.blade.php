<div class="ad__sort btn-group">
  <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <div v-if="query.sortBy === 1 || query.sortBy === 2 || query.sortBy === 3  || query.sortBy === 4  || query.sortBy === 5 || query.sortBy === 6" v-cloak>
      <div v-if="query.sortBy === 1">
         {{ trans('home.filter.ad_show') }}
             <b>{{ trans('home.filter.ad_show_new') }}</b>
      </div>
      <div v-if="query.sortBy === 2">
         {{ trans('home.filter.ad_show') }}
             <b>{{ trans('home.filter.ad_show_edited') }}</b>
      </div>
      <div v-if="query.sortBy === 3">
         {{ trans('home.filter.ad_show') }}
             <b>{{ trans('home.filter.ad_show_discount') }}</b>
      </div>
      <div v-if="query.sortBy === 4">
         {{ trans('home.filter.ad_show') }}
             <b>{{ trans('home.filter.ad_show_expensive') }}</b>
      </div>
      <div v-if="query.sortBy === 5">
         {{ trans('home.filter.ad_show') }}
             <b>{{ trans('home.filter.ad_show_alphabet_az') }}</b>
      </div>
      <div v-if="query.sortBy === 6">
         {{ trans('home.filter.ad_show') }}
             <b>{{ trans('home.filter.ad_show_alphabet_za') }}</b>
      </div>
  </div>
  <div v-else>
    <div v-if="query.sortBy === 0" v-cloak>
      {{ trans('home.filter.ad_show') }}
         <b>{{ trans('home.filter.all_select') }}</b>
    </div>
    <div v-else>
      {{ trans('home.filter.ad_filter') }}
      <span class="caret"></span>
    </div>
  </div>
  </button>
  <ul class="dropdown-menu">
    <li>
      <a v-bind:class="{ active: query.sortBy === 0}" href="#"
        @click="toggleSortBy(0)">
        {{ trans('home.filter.all_select') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.sortBy === 1}" href="#"
        @click="toggleSortBy(1)">{{ trans('home.filter.ad_new_top') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.sortBy === 2}" href="#"
        @click="toggleSortBy(2)">{{ trans('home.filter.ad_edited_top') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.sortBy === 3}" href="#"
        @click="toggleSortBy(3)">{{ trans('home.filter.ad_discount_top') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.sortBy === 4}" href="#"
        @click="toggleSortBy(4)">{{ trans('home.filter.ad_expensive_top') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.sortBy === 5}" href="#"
        @click="toggleSortBy(5)">{{ trans('home.filter.ad_by_alphabet_az') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.sortBy === 6}" href="#"
        @click="toggleSortBy(6)">{{ trans('home.filter.ad_by_alphabet_za') }}</a>
    </li>
  </ul>
</div>
