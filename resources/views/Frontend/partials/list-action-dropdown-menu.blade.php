<div class="ad__action btn-group">
  <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <div v-if="query.action === 1 || query.action === 2" v-cloak>
       <div v-if="query.action === 1">
          <b>{{ trans('home.filter.proposes') }}</b>
       </div>
       <div v-if="query.action === 2">
          <b>{{ trans('home.filter.looking') }}</b>
       </div>
    </div>
    <div v-else>
      <div v-if="query.action === 0" v-cloak>
         <b>{{ trans('home.filter.all_select') }}</b>
      </div>
      <div v-else>
        {{ trans('home.filter.proposes_looking_select') }}
        <span class="caret"></span>
      </div>
    </div>
  </button>
  <ul class="dropdown-menu">
    <li>
      <a v-bind:class="{ active: query.action === 0}" href="#"
        @click="toggleAction(0)">
        {{ trans('home.filter.all_select') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.action === 1}" href="#"
        @click="toggleAction(1)">{{ trans('home.filter.proposes') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.action === 2}" href="#"
        @click="toggleAction(2)">{{ trans('home.filter.looking') }}</a>
    </li>
  </ul>
</div>
