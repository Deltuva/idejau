<div class="ad__plus btn-group">
  <div class="btn btn-default btn-sm dropdown-toggle"
    @click="toggleActiveFilter()"
    v-bind:class="{ active: isActiveFilter}">
    <i class="fa fa-search-plus" aria-hidden="true"></i>
  </div>
</div>
