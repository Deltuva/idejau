@if (!is_null($adhead['category_title']))
  {{ $adhead['category_title'] }}
     <span class="count-badge">{{ $adhead['category_ad_count'] }}</span>
   @else
     {{ trans('home.ad.list') }} <span class="count-badge">{{ $adhead }}</span>
@endif
