<div class="cl-ad__breadcrumbs">
  <ol class="breadcrumb">
     @foreach (\DM\Helpers\Website::getBreadcrumbs() as $br)
       @if ($br)
         @if ($br['breadcrumbs_url'] == 'home')
           <li>
              <i class="fa fa-home" aria-hidden="true"></i> <a href="{{ url('/') }}">
                @if (!is_null($br['breadcrumbs_title']))
                   {{ $br['breadcrumbs_title'] }}
                @endif
              </a>
           </li>
         @else
           <li>
              <a href="{{ url('skelbimai',$br['breadcrumbs_url']) }}">
                @if (!is_null($br['breadcrumbs_title']))
                   {{ $br['breadcrumbs_title'] }}
                @endif
              </a>
           </li>
         @endif
       @endif
     @endforeach
  </ol>
</div>
