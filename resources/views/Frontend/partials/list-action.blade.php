<div class="ad__action btn-group">
  <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  @if(request()->get('action') == '1' or request()->get('action') == '2')
     {!! request()->get('action') == '1' ? '<b>'.trans('home.filter.proposes').'</b>':'' !!}
     {!! request()->get('action') == '2' ? '<b>'.trans('home.filter.looking').'</b>':'' !!}
  @else
    {{ trans('home.filter.proposes_looking_select') }}
    <span class="caret"></span>
  @endif
  </button>
  <ul class="dropdown-menu">
    <li>
      <a href="{!! url()->current() !!}">
        {{ trans('home.filter.all_select') }}</a>
    </li>
    <li>
      <a {!! request()->get('action') == '1' ? 'class="active"' : '' !!} href="{!!
        request()->fullUrlWithQuery([
           'action' => '1'
        ]) !!}">{{ trans('home.filter.proposes') }}</a>
    </li>
    <li>
      <a {!! request()->get('action') == '2' ? 'class="active"' : '' !!} href="{!!
        request()->fullUrlWithQuery([
           'action' => '2'
        ]) !!}">{{ trans('home.filter.looking') }}</a>
    </li>
  </ul>
</div>
