<div class="ad__type btn-group">
  <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  @if(request()->get('userType') == '1' or request()->get('userType') == '2')
     {!! request()->get('userType') == '1' ? '<b>'.trans('home.filter.person').'</b>':'' !!}
     {!! request()->get('userType') == '2' ? '<b>'.trans('home.filter.company').'</b>':'' !!}
  @else
    {{ trans('home.filter.person_company_select') }}
    <span class="caret"></span>
  @endif
  </button>
  <ul class="dropdown-menu">
    <li>
      <a href="{!! url()->current() !!}">
        {{ trans('home.filter.all_select') }}</a>
    </li>
    <li>
      <a {!! request()->get('userType') == '1' ? 'class="active"' : '' !!} href="{!!
        request()->fullUrlWithQuery([
           'userType' => '1'
        ]) !!}">{{ trans('home.filter.person') }}
        @if (!is_null($adhead['category_title']))
           <span class="count-badge pull-right">{{ $adhead['category_ad_private_count'] }}</span>
        @else
           <span class="count-badge pull-right">{{ $adhead_private_count }}</span>
        @endif
        </a>
    </li>
    <li>
      <a {!! request()->get('userType') == '2' ? 'class="active"' : '' !!} href="{!!
        request()->fullUrlWithQuery([
           'userType' => '2'
        ]) !!}">{{ trans('home.filter.company') }}
        @if (!is_null($adhead['category_title']))
           <span class="count-badge pull-right">{{ $adhead['category_ad_company_count'] }}</span>
        @else
           <span class="count-badge pull-right">{{ $adhead_company_count }}</span>
        @endif
      </a>
    </li>
  </ul>
</div>
