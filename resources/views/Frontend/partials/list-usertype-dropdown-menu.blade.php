<div class="ad__type btn-group">
  <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <div v-if="query.userType === 1 || query.userType === 2" v-cloak>
      <div v-if="query.userType === 1">
        <b>{{ trans('home.filter.person') }}</b>
      </div>
      <div v-if="query.userType === 2">
        <b>{{ trans('home.filter.company') }}</b>
      </div>
  </div>
  <div v-else>
    <div v-if="query.userType === 0" v-cloak>
      {{ trans('home.filter.ad_show') }}
         <b>{{ trans('home.filter.all_select') }}</b>
    </div>
    <div v-else>
      {{ trans('home.filter.person_company_select') }}
      <span class="caret"></span>
    </div>
  </div>
  </button>
  <ul class="dropdown-menu">
    <li>
      <a v-bind:class="{ active: query.userType === 0}" href="#"
        @click="toggleUserType(0)">
        {{ trans('home.filter.all_select') }}</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.userType === 1}" href="#"
        @click="toggleUserType(1)">{{ trans('home.filter.person') }}
        (0)</a>
    </li>
    <li>
      <a v-bind:class="{ active: query.userType === 2}" href="#"
        @click="toggleUserType(2)">{{ trans('home.filter.company') }}
        (0)</a>
    </li>
  </ul>
</div>
