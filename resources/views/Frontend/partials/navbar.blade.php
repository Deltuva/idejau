<!-- Navigation -->
<nav class="navbar-default navbar-static-top">
  <div class="container">
      <div class="navbar-header">
        <a href="/"><img class="hidden-xs responsive-logo" src="{{ asset('images/laravel.png') }}" width="180" alt="logotipas"></a>
        <!-- Hamburger animation btn -->
            <button type="button" @click="toggleMobileMeniu" class="navbar-toggle"
                v-bind:class="{ active: isActiveMobileMeniu}">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
        <!-- /Hamburger animation btn -->
      </div>
    <div class="collapse navbar-collapse">
      <ul class="hidden-xs nav navbar-nav navbar-right">
        <li class="dropdown">
          <ul class="hidden-xs nav navbar-nav navbar-right">
            <!-- Login block -->
            @if (!Auth::check())
              <li><a class="btn-primary" href="{{ url('login') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ trans('home.navbar.new_post') }}</a>
              </li>
              <li><a class="btn-primary" href="{{ url('login') }}">
                <i class="fa fa-user" aria-hidden="true"></i> {{ trans('home.navbar.login_btn') }}</a>
              </li>
            @else
              <!-- Logged in meniu -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expa="false">
                    <span class="username" style="color: black;">
                      {{ Auth::user()->getFullUserName() }}</span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a><i class="fa fa-btn fa-sign-out"></i>
                      {{ trans('home.navbar.logout_link') }}</a></li>
                </ul>
              </li>
              <!-- /Logged in meniu -->
            @endif
            <!-- /Login block -->
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div v-if="isActiveMobileMeniu" class="visible-xs">
    <nav class="nav-mnu">
      <ul>
          <li><a href="#">{{ trans('home.navbar_mobile.info') }}</a></li>
          @if(Auth::check())
            <li><a href="#">{{ trans('home.navbar_mobile.remembers') }}
                <dm-favorite-count :total="{{ $countUserFavorites }}" :web="'responsive'" v-cloak></dm-favorite-count></a></li>
          @endif

          @if (!Auth::check())
            <li><a href="#">{{ trans('home.navbar_mobile.login') }}</a></li>
            <li><a href="#">{{ trans('home.navbar_mobile.register') }}</a></li>
          @else
            <li><a href="#">{{ trans('home.navbar_mobile.my_meniu') }}</a></li>
            <li><a href="#">{{ trans('home.navbar_mobile.my_services') }}</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="username">Mindaugas</span> <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#"><i class="fa fa-btn fa-sign-out"></i> {{ trans('home.navbar_mobile.logout') }}</a></li>
                </ul>
            </li>
          @endif
      </ul>
    </nav>
</div>
<!-- /Navigation -->
<div class="top-menu-line clearfix">
  <div class="container">
    <div class="row">
      <div class="hidden-xs hidden-sm col-md-6">
        <ul class="menu-links">
            <li><a href="#">{{ trans('home.navbar.new_post') }}</a></li>
            <li>
               <a href="{{ url('patike_skelbimai') }}">{{ trans('home.navbar.me_favorites_posts') }}</a>
               @if(Auth::check())
                  <dm-favorite-count :total="{{ $countUserFavorites }}" :web="'dekstopscreen'" v-cloak></dm-favorite-count>
               @endif
            </li>
            <li><a href="#">{{ trans('home.navbar.advertsment') }}</a></li>
            <li><a href="#">{{ trans('home.navbar.rules') }}</a></li>
        </ul>
      </div>
      {{-- <div class="col-xs-6 col-lg-2 col-md-3 col-sm-4 text-right pull-right">
        <div class="cl-add-button">
            <i class="fa fa-plus" aria-hidden="true"></i> <a href="#">{{ trans('home.navbar.add_new_post') }}</a>
        </div>
      </div> --}}
    </div>
  </div>
</div>
