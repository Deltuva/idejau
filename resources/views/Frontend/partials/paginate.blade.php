<nav v-cloak>
    <ul class="pagination">
        <li v-if="meta.paginate.current_page > 1">
            <a href="#" aria-label="Previous"
               @click.prevent="changePage(meta.paginate.current_page - 1)">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        <li v-for="page in pagesNumber"
            v-bind:class="[ page == isActived ? 'active' : '']">
            <a href="#"
               @click.prevent="changePage(page)">@{{ page }}</a>
        </li>
        <li v-if="meta.paginate.current_page < meta.paginate.last_page">
            <a href="#" aria-label="Next"
               @click.prevent="changePage(meta.paginate.current_page + 1)">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>
