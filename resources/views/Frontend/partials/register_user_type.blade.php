<span :class="{ user_active: user.user_type}">
    <span v-if="user.user_type == 1" v-cloak>
        {{ trans('home.register_user_type_selected') }}
        {{ trans('home.register_user_type_private') }}</span>
    <span v-if="user.user_type == 2" v-cloak>
        {{ trans('home.register_user_type_selected') }}
        {{ trans('home.register_user_type_company') }}</span>
</span>
