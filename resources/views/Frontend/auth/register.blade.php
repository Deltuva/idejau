@extends('frontend/layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8">
            <div class="register">
              <div class="register-head">{{ trans('home.register2_title') }}</div>

                <div class="register-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                          <label class="control-label radio-inline">
                              <input type="radio" name="user_type" value="1">
                                  {{ trans('home.register_user_type_private') }}
                          </label>

                          <label class="control-label radio-inline">
                              <input type="radio" name="user_type" value="2">
                                  {{ trans('home.register_user_type_company') }}
                          </label>
                          {{-- @include('partials.register_user_type') --}}

                          @if ($errors->has('user_type'))
                              <span class="help-block">
                                  <i class="fa fa-exclamation-circle" aria-hidden="true"></i> {{ $errors->first('user_type') }}
                              </span>
                          @endif
                       </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">{{ trans('home.register_email') }}:</label>

                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> {{ $errors->first('email') }}
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">{{ trans('home.register_password') }}:</label>

                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> {{ $errors->first('password') }}
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="control-label">{{ trans('home.register_password2') }}:</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <i class="fa fa-exclamation-circle" aria-hidden="true"></i> {{ $errors->first('password_confirmation') }}
                                    </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="subscribe" {{ old('subscribe') ? 'checked' : ''}}> 
                                    <span style="margin: 0 10px;">{{ trans('home.register_subscribe') }}</span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="terms_agree" {{ old('terms_agree') ? 'checked' : ''}}> 
                                    <span style="margin: 0 10px;">{{ trans('home.register_terms') }}</span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn register-btn">
                                {{ trans('home.register_btn') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
