@extends('frontend/layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="cl-post-block">
          <div v-for="aditem in adlist">
            @{{ aditem }}
            <div class="cl-post__item" v-cloak>
              <div class="row">
                <div class="col-xs-4 col-sm-2 post__image">
                  <a href="#">
                    <img class="img-responsive"
                      :src="aditem.pic.image"
                      alt="{{ trans('home.ad.thumb_loading') }}"></a>
                    <div class="vt-pic" v-html="aditem.pic.total_images">
                    </div>
                </div>
                <div class="col-xs-8 col-sm-10 post__content">
                  <h3><a target="_blank" href="#">@{{ aditem.title }}</a></h3>
                  <div class="post__price">
                    <span class="price"> @{{ aditem.price }} &euro;
                    </span>
                  </div>
                  <div class="clearfix"></div>
                  <div class="description">
                    @{{ aditem.description }}
                    <div class="hidden-xs">
                        <p v-html="aditem.more"></p>
                    </div>
                  </div>
                  <div class="location hidden-xs">
                    @{{ aditem.location }}
                  </div>
                  <div class="more-f hidden-xs">
                    <div v-if="aditem.utype == 'company'">
                      <span class="company">
                        <a href="/"><img
                          :src="aditem.company.image"
                          :alt="aditem.company.name"></a>
                      </span>
                    </div>
                    <div v-else>
                        <div class="favorite">
                          <i class="fa fa-heart-o" aria-hidden="true"></i>
                          <span class="fav-h-t">
                            {{ trans('home.ad.remember') }}
                          </span>
                        </div>
                        <span class="created" v-html="aditem.ago_edit"></span>
                        <span class="created" v-html="aditem.ago_created"></span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
          <vue-pagination v-bind:pagination="pagination"
                 v-on:click.native="getUsers(pagination.current_page)"
                 :offset="4">
          </vue-pagination>
        </div>
      </div>
    </div>
  </div>
@endsection
