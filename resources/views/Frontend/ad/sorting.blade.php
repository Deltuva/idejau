<div class="ad__count pull-left">
  @include('frontend/partials.list-userType')
  <div class="ad__name">
    {{-- @if (request()->is('skelbimai/*/automobiliai') || request()->is('skelbimai/*/automobiliai/*'))
        automobiliai
    @endif --}}
    <h1>
      @include('frontend/partials.list-category-title')
    </h1>
  </div>
</div>
<div class="ad__count pull-right">
    @include('frontend/partials.list-action')
    {{-- @include('frontend/partials.list-orderby-dropdown-menu') --}}
    @include('frontend/partials.list-more-toggle-menu')
</div>
