@extends('frontend/layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="cl-post-block">

				{{-- @if(isset($categoriesList) && !empty($categoriesList))
					<div class="cl-post__categories">
            <ul class="child">
              @foreach($categoriesList as $c)
                @if(!is_null($c->name))
                  <li>
										<a href="{{ url("skelbimai",$c->categorySlugAt) }}">{{ $c->categoryNameReplaceAt }}
                      <span class="count-badge">({{ $c->countOfCategoriesAt }})</span></a>
                  </li>
                @endif
              @endforeach
            </ul>
          </div>
				@endif --}}

          <div class="cl-post__options">
            <div class="post__count pull-left">
							{{-- @include('partials/post-usertype-dropdown-menu') --}}
              <div class="post__name">
                <h1>
                  Skelbimai
                  <span>(0)</span>
                </h1>
              </div>
            </div>
            {{-- <div class="post__count pull-right">
              @include('partials/post-action-dropdown-menu')
              @include('partials/post-orderby-dropdown-menu')
              @include('partials/post-more-toggle-menu')
            </div> --}}
            <div class="clearfix"></div>
          </div>

          @if ($adList->count() > 0)
            @foreach($adList as $adItem)
              <div class="cl-post__item">
                <div class="row">
                  <div class="col-xs-4 col-sm-2 post__image">
                      <a href="#">
                        <img class="img-responsive
                        {{ $adItem->present()
                          ->latestVisitStatus ? 'visited' : null }}"
                        src="{{ $adItem->present()
                          ->adImage }}" alt="{{ trans('home.ad.thumb_loading') }}"></a>
                        <div class="vt-pic">
                          {!! $adItem->present()
                            ->adImageCount !!}
                        </div>
                      @if ($adItem->present()
                        ->latestVisitStatus)
                        <div class="vt-bt">
                          {{ trans('home.ad.latest_visited_status') }}
                        </div>
                      @endif
                  </div>
                  <div class="col-xs-8 col-sm-10 post__content">
                    <h3><a target="_blank" href="#">
                      {!! $adItem->present()
                        ->adTitleName !!}</a></h3>
                    <div class="post__price">
                      <span class="price">
                         {{ $adItem->ad_price }} &euro;
                      </span>
                      {!! $adItem->present()
                        ->starsCountWidget !!}
                      {!! $adItem->present()
                        ->todayAdWidget !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="description">
                      {{ $adItem->present()
                        ->limitDescription }}
                      <div class="hidden-xs">
                        <p>{!! $adItem->present()
                          ->viewByCategory !!}</p>
                      </div>
                    </div>
                    <div class="location hidden-xs">
                      {!! $adItem->present()
                        ->locationName !!}
                    </div>
                    <div class="more-f hidden-xs">
                      @if ($adItem->user_type == 'company')
                        <span class="company">
                          <a href="/"><img src="{{ $adItem->present()
                            ->adCompanyImage }}" alt="{{ $adItem->present()
                              ->adCompanyName }}"></a>
                        </span>
                      @else
                          <div class="favorite">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <span class="fav-h-t">
                              {{ trans('home.ad.remember') }}
                            </span>
                          </div>
                          <span class="created">
                            {!! $adItem->present()
                              ->timeAgoEditedStatus !!}
                            {!! $adItem->present()
                              ->timeAgoDays !!}
                          </span>
                      @endif
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            @endforeach
            @if ($adList->lastPage() > 0)
                {!! $adList->appends(request()->query())
                    ->render() !!}
            @endif
          @else
              <div class="empty__posts">
                  <p>{{ trans('home.ad.no_posts') }}.</p>
              </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection
