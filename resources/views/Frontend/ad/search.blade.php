<div class="cl-filter__meniu" v-show="isActiveFilter" v-cloak>
  <div class="col-xs-12 price-filter pull-left">
    {!! Form::open(['url' => request()->fullUrl(),'method' => 'GET', 'class' => 'disable-empty-fields', 'id' => 'cl-filter']) !!}
      <div class="f-fltr">
        <input type="hidden" name="way" value="nekilnojamasis-turtas">
        <dm-car-component></dm-car-component>
        <div class="form-group row">
          <dm-all-price-select></dm-all-price-select>
            <div class="col-xs-12 col-md-2">
              <button type="submit" class="pull-right btn btn-primary form-control"> <i class="fa fa-search-plus" aria-hidden="true"></i> {{ trans('home.filter_data.filter_btn') }}</button>
            </div>
        </div>
      </div>
    {!! Form::close() !!}
  </div>
  <div class="clearfix"></div>
</div>