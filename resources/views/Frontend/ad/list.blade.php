@extends('frontend/layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        @include('frontend/partials.list-breadcrumbs')
        <div class="cl-ad-block">
          {{-- @if (\Route::current()->getName() == 'c0') --}}
            @if(isset($adcategorieslist) && !empty($adcategorieslist))
    					<div class="cl-ad__categories">
                <ul class="child">
                  @foreach($adcategorieslist as $category)
                    @if(!is_null($category->name))
                      <li>
    										<a href="{!! url('skelbimai',$category->categorySlugAt) !!}">{{ $category->categoryNameReplaceAt }}
                          <span class="count-badge">({{ $category->countOfCategoriesAt }})</span></a>
                      </li>
                    @endif
                  @endforeach
                </ul>
              </div>
    				@endif
          {{-- @elseif (\Route::current()->getName() == 'c1' || \Route::current()->getName() == 'c2') --}}
            @if(isset($adsubcategorieslist) && !empty($adsubcategorieslist))
    					<div class="cl-ad__categories">
                <ul class="child">
                  @foreach($adsubcategorieslist as $subcategory)
                    @if(!is_null($subcategory->name))
                      <li>
    										<a href="{!! url('skelbimai', $subcategory->subcategorySlugAt) !!}">{{ $subcategory->categoryNameReplaceAt }}
                          <span class="count-badge">({{ $subcategory->countOfSubCategoriesAt }})</span></a>
                      </li>
                    @endif
                  @endforeach
                </ul>
              </div>
    				@endif
          {{-- @elseif (\Route::current()->getName() == 'c1' || \Route::current()->getName() == 'c2' || \Route::current()->getName() == 'c3') --}}
            @if(isset($adsubsubcategorieslist) && !empty($adsubsubcategorieslist))
              <div class="cl-ad__categories">
                <ul class="child">
                  @foreach($adsubsubcategorieslist as $subsubcategory)
                    @if(!is_null($subsubcategory->name))
                      <li>
                        <a href="{{ url('skelbimai',$subsubcategory->subcategorySlugAt) }}">{{ $subsubcategory->categoryNameReplaceAt }}
                          <span class="count-badge">({{ $subsubcategory->countOfSubSubCategoriesAt }})</span></a>
                      </li>
                    @endif
                  @endforeach
                </ul>
              </div>
            @endif
          {{-- @endif --}}
          @include('frontend/ad.search')

          <div class="cl-ad__options">
            @include('frontend/ad.sorting')

            <div class="clearfix"></div>
          </div>
          @if ($adlist->count() > 0)
            @foreach($adlist as $adItem)
              <div class="cl-ad__item">
                <div class="row">
                  <div class="col-xs-4 col-sm-2 ad__image">
                    <a href="#">
                      <img class="img-responsive" src="{{ $adItem->getImage() }}" alt="{{ trans('home.ad.thumb_loading') }}"></a>
                        <div class="vt-pic">
                          {!! $adItem->getImageCount() !!}
                        </div>
                      {{-- @if ($adItem->getLatestVisitStatus())
                        <div class="vt-bt">
                          {{ trans('home.ad.latest_visited_status') }}
                        </div>
                      @endif --}}
                  </div>
                  <div class="col-xs-8 col-sm-10 ad__content">
                    <h3><a target="_blank" href="#">
                      {!! $adItem->getTitleName() !!}</a></h3>
                    <div class="ad__price">
                      <span class="price">
                         {{ $adItem->ad_price }} &euro;
                      </span>
                      {!! $adItem->getStarsCountWidget() !!}
                      {!! $adItem->getTodayWidget() !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="description">
                      {{ $adItem->getLimitDescription() }}
                      <div class="hidden-xs">
                        <p>{!! $adItem->getMoreDescription() !!}</p>
                      </div>
                    </div>
                    <div class="location hidden-xs">
                      {{ $adItem->getLocationName() }}
                    </div>
                    <div class="more-f hidden-xs">
                      <dm-favorite favorite="{{ $adItem->ad_id }}" isfavorited="{{ $adItem->isFavorited() }}">
                      </dm-favorite>
                      {!! $adItem->getCompanyDetail() !!}
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            @endforeach
            @if ($adlist->lastPage() > 0)
                {!! $adlist->appends(request()->query())
                    ->render() !!}
            @endif
          @else
              <div class="alert alert-danger text-center" style="margin-top: 5px;">
                {{ trans('home.ad.no_ads') }}.
              </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection
