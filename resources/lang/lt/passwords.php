<?php

return [
    'password_reset_title'       => 'Slaptažodžio atkūrimas',
    'password_reset_email_btn'   => 'Siųsti atkūrimo nuorodą',
    'password_reset_btn'         => 'Atkurti slaptažodį',
    'password'                   => 'Slaptažodis turi būti bent šešių simbolių ir sutapti su patvirtinimu.',
    'reset'                      => 'Nustatytas naujas slaptažodis!',
    'sent'                       => 'Naujo slaptažodžio nustatymo nuoroda išsiųsta',
    'token'                      => 'Šis slaptažodžio raktas yra neteisingas.',
    'user'                       => 'Vartotojas su tokiu el. paštu nerastas.',
];
