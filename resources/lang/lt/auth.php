<?php

return [
    'login_title'     => 'Prisijungimas',
    'email'           => 'El.paštas',
    'password'        => 'Slaptažodis',
    'remember'        => 'Atsiminti mane',
    'login_btn'       => 'Prisijungti',
    'password_forget' => 'Pamiršote savo slaptažodį?',
    'failed'          => 'Prisijungimo duomenys neatitinka.',
    'throttle'        => 'Perdaug bandymų prisijungti. Bandykite po :seconds sec.',
];
