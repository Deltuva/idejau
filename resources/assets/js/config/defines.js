/**
 * Defines constants
 */
export const __DM_API_DOMAIN = 'http://localhost:8000/'

// Car
export const TYPE_MAKE = 'makes';
export const TYPE_MODEL = 'models';

export const AXIOS_CAR_MAKES_POINT = '/api/carMakes/';
export const AXIOS_CAR_MAKES_MODEL_POINT = '/api/carModels/';
export const AXIOS_CAR_BODYS_POINT = '/api/carBodys/';
export const AXIOS_CAR_COLORS_POINT = '/api/carColors/';
export const AXIOS_CAR_DAMAGES_POINT = '/api/carDamages/';
export const AXIOS_CAR_ENGINES_POINT = '/api/carEngines/';
export const AXIOS_CAR_FUELS_POINT = '/api/carFuels/';
export const AXIOS_CAR_GEARBOXES_POINT = '/api/carGearboxes/';
export const AXIOS_CAR_YEARS_POINT = '/api/carYears/';
export const AXIOS_CAR_POWERS_POINT = '/api/carPowers/';
export const AXIOS_CAR_RIDAS_POINT = '/api/carRidas/';
export const AXIOS_CAR_WHEELS_POINT = '/api/carWheels/';

// All
export const AXIOS_PRICES_POINT = '/api/Prices/';
export const AXIOS_FAVORITE_POINT = '/favorite/';
export const AXIOS_UNFAVORITE_POINT = '/unfavorite/';
export const AXIOS_FAVORITE_COUNT_POINT = '/favorite_count/';
