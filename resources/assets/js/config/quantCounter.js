/**
 * QuantCounter
 */

class QuantCounter {

    // jquery quant counter plus
    static plus() {

        let quantPlace = $('span.ads, span.remember_ads');
        let quantCount = $('span[class="ads"], span[class="remember_ads"]').html();
        let currentQuant = parseInt(quantPlace.html());

        currentQuant += 1;
        quantPlace.html(Math.abs(currentQuant));
    }

    // jquery quant counter minus
    static minus() {

        let quantPlace = $('span.ads, span.remember_ads');
        let quantCount = $('span[class="ads"], span[class="remember_ads"]').html();
        let currentQuant = parseInt(quantPlace.html());

        currentQuant -= 1;
        quantPlace.html(Math.abs(currentQuant));
    }
}

export default QuantCounter;