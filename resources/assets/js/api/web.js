/**
 * API
 */

class Api {

  // Car
  static getMakes(then) {
    return axios(config.AXIOS_CAR_MAKES_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getModels(then, makeId) {
    return axios(config.AXIOS_CAR_MAKES_MODEL_POINT + makeId)
      .then(response => then(response.data.data))
      .catch()
  }

  static getBodys(then) {
    return axios(config.AXIOS_CAR_BODYS_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getColors(then) {
    return axios(config.AXIOS_CAR_COLORS_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getDamages(then) {
    return axios(config.AXIOS_CAR_DAMAGES_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getEngines(then) {
    return axios(config.AXIOS_CAR_ENGINES_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getFuels(then) {
    return axios(config.AXIOS_CAR_FUELS_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getGearboxes(then) {
    return axios(config.AXIOS_CAR_GEARBOXES_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getYears(then) {
    return axios(config.AXIOS_CAR_YEARS_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getPowers(then) {
    return axios(config.AXIOS_CAR_POWERS_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getRidas(then) {
    return axios(config.AXIOS_CAR_RIDAS_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static getWheels(then) {
    return axios(config.AXIOS_CAR_WHEELS_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  // All
  static getPrices(then) {
    return axios(config.AXIOS_PRICES_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  static favorite(favId) {
    return axios(config.AXIOS_FAVORITE_POINT + favId)
      .then(response => this.isFavorited = true)
      .catch(response => console.log(response))
  }

  static unfavorite(favId) {
    return axios(config.AXIOS_UNFAVORITE_POINT + favId)
      .then(response => this.isFavorited = false)
      .catch(response => console.log(response))
  }

  static getCountUserFavorites(then) {
    return axios(config.AXIOS_FAVORITE_COUNT_POINT)
      .then(response => then(response.data.data))
      .catch()
  }
}

export default Api;