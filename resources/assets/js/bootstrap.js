window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
try {
    window.$ = window.jQuery = require('jquery');
    require('bootstrap-sass');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.Vue = require('vue');
require('vue-resource');

window.axios = require('axios');

axios.defaults.baseURL = 'http://localhost:8000';
window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};
window.VueAxios = require('vue-axios');

// Vue.http.interceptors.push((request, next) => {
//     // request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);
//     request.headers.set('X-Socket-Id', Echo.socketId());

//     next();
// });

/**
 * Project services
 */

import * as config from './config/defines.js';
window.config = config;

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');
Pusher.logToConsole = true;

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'f86f3b8e574b4f24551a',
    cluster: 'eu',
    encrypted: true
});