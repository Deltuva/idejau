/**
 * Notification Message
 */

class NotificationMessage {

    static newNotify(title, message, icon = "http://localhost:8000/images/photos/audi2.jpg") {
        if (!('Notification' in window)) {
            alert('Web Notification is not supported');
            return;
        }
        Notification.requestPermission(permission => {
            let notification = new Notification(title, {
                body: message,
                icon: icon
            });
            notification.onclick = () => {
                window.open(window.location.href);
            };
        });
    }
}

export default NotificationMessage;