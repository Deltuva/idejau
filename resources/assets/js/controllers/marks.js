// new Vue({
//     data() {
//         return {
//             marks: [],
//             formVariables: {
//                 isMark_id: '',
//                 isModelDisabled: true
//             },
//             models: [],
//             loading: false
//         }
//     },
//     created() {
//         this.fetchCarMarks();
//     },
//     watch: {
//         'formVariables.isMark_id': {
//             handler: function(markId) {
//               if(!markId) {
//                 return;
//               }
//                 this.fetchCarMarksModels(markId);
//             },
//             immediate: true,
//             deep: true
//         }
//     },
//     methods: {
//         fetchCarMarks() {
//             this.loading = true;
//             this.$http.get('http://laravel.dev/api/carmarks')
//                 .then((response) => {
//                     this.loading = false;
//                     this.marks = JSON.parse(response.body);
//                 }, (response) => {
//                     // error callback
//                 });
//         },
//         fetchCarMarksModels(markId) {
//             this.loading = true;
//             this.$http.get('http://laravel.dev/api/carmarksmodels/' + markId)
//                 .then((response) => {
//                     this.loading = false;
//                     this.models = JSON.parse(response.body);
//                 }, (response) => {
//                     // error callback
//                 });
//         }
//     }
// });
