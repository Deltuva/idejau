require('./bootstrap');
import DmCarComponent from './components/dm_car_component.vue';
import DmAllPriceSelect from './components/dm_all_price_select.vue';
import DmFavorite from './components/dm_favorite.vue';
import DmFavoriteCount from './components/dm_favorite_count.vue';

Vue.config.debug = true;
Vue.config.unsafeDelimiters = ['{!!', '!!}'];
Vue.config.devtools = true;
Vue.config.productionTip = false;

Vue.use(VueAxios, axios);

/* ==========================================================================
    Cut string ...
========================================================================== */
const cutFilter = function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('cut', cutFilter);

const app = new Vue({
    el: '#app',
    data: {
        isActiveFilter: false,
        isActiveMobileMeniu: false
    },
    methods: {
        toggleActiveFilter() {
            this.isActiveFilter = !this.isActiveFilter;
        },
        toggleMobileMeniu() {
            this.isActiveMobileMeniu = !this.isActiveMobileMeniu;
        }
    },
    components: {
      DmCarComponent,
      DmAllPriceSelect,
      DmFavorite,
      DmFavoriteCount
    }
});

$(function() {
  /* ==========================================================================
      Form clear query strings
  ========================================================================== */
  $("form.disable-empty-fields").on("submit", function() {
    $(this).find(":input:not(button)").filter(function() {
      return !this.value
    }).prop("disabled", !0);
    return !0
  }).find(":input:not(button)").prop("disabled", !1);
});
