<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// Car
Route::get('/carMakes', ['uses' => 'Frontend\VueJs\CAR_MakeController@getCollection']);
Route::get('/carModels/{makeId?}', ['uses' => 'Frontend\VueJs\CAR_ModelController@getCollection']);
Route::get('/carPowers', ['uses' => 'Frontend\VueJs\CAR_PowerController@getCollection']);
Route::get('/carFuels', ['uses' => 'Frontend\VueJs\CAR_FuelController@getCollection']);
Route::get('/carGearboxes', ['uses' => 'Frontend\VueJs\CAR_GearboxController@getCollection']);
Route::get('/carRidas', ['uses' => 'Frontend\VueJs\CAR_RidaController@getCollection']);
Route::get('/carBodys', ['uses' => 'Frontend\VueJs\CAR_BodyController@getCollection']);
Route::get('/carWheels', ['uses' => 'Frontend\VueJs\CAR_WheelController@getCollection']);
Route::get('/carYears', ['uses' => 'Frontend\VueJs\CAR_YearController@getCollection']);
Route::get('/carEngines', ['uses' => 'Frontend\VueJs\CAR_EngineController@getCollection']);
Route::get('/carColors', ['uses' => 'Frontend\VueJs\CAR_ColorController@getCollection']);
Route::get('/carDamages', ['uses' => 'Frontend\VueJs\CAR_DamageController@getCollection']);

// All
Route::get('/Prices', ['uses' => 'Frontend\VueJs\PriceController@getCollection']);