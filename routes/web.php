<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

$api = app('Dingo\Api\Routing\Router');

// Frontend
Auth::routes();
Route::get('/', ['uses' => 'Frontend\PageController@pageHome', 'as' => 'frontHome']);
Route::get('/promotion', ['uses' => 'Frontend\PageController@pagePromotion', 'as' => 'frontPromotion']);
Route::get('/term', ['uses' => 'Frontend\PageController@pageTerm', 'as' => 'frontTerm']);
Route::get('/about', ['uses' => 'Frontend\PageController@pageAbout', 'as' => 'frontAbout']);
Route::get('/patike_skelbimai', ['middleware' => 'auth', 'uses' => 'Frontend\PageController@pageUserFavorites', 'as' => 'frontUserFavorites']);
Route::get('/duk', ['uses' => 'Frontend\PageController@pageDuk', 'as' => 'frontDuk']);
Route::get('/developer', ['uses' => 'Frontend\PageController@pageDeveloper', 'as' => 'frontDeveloper']);
Route::get('/skelbimai', ['uses' => 'Frontend\AdController@index', 'as' => 'c0']);
Route::get('/skelbimai/{c1}', ['uses' => 'Frontend\AdController@indexCat', 'as' => 'c1']);
Route::get('/skelbimai/{c1}/{c2}', ['uses' => 'Frontend\AdController@indexSub', 'as' => 'c2']);
Route::get('/skelbimai/{c1}/{c2}/{c3}', ['uses' => 'Frontend\AdController@indexSubsub', 'as' => 'c3']);
Route::get('/demo/{s1}/{s2}/{s3}', ['uses' => 'Frontend\DemoController@index', 'as' => 'frontDemo']);
Route::get('/search', 'Frontend\AdController@search')->name('search');
Route::get('/favorite/{favId}', ['uses' => 'Frontend\VueJs\FavoriteController@favoriteNewAd', 'as' => 'favoriteAd']);
Route::get('/unfavorite/{favId}', ['uses' => 'Frontend\VueJs\FavoriteController@unfavoriteNewAd', 'as' => 'unfavoriteAd']);
Route::get('/favorite_count', ['uses' => 'Frontend\VueJs\FavoriteController@countAllUserFavorites', 'as' => 'favoritesCountAd']);
Route::get('/ok', ['uses' => 'Frontend\VueJs\FavoriteController@ok', 'as' => 'oky']);

$api->version('v1', [], function ($api) {
    $api->get('skelbimai', 'DM\Http\Controllers\Frontend\AdController@getAdListingIndex');
});
