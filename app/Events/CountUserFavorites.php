<?php

namespace DM\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use DM\Repositories\V1\FavoriteRepository;

class CountUserFavorites implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var FavoriteRepository
     */
    public $favorites;

    /**
     * Constructor
     */
    public function __construct(FavoriteRepository $favorites)
    {
        $this->favorites = $favorites;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('favorites');
    }

    /**
     * Count favorites
     *
     * @return void
     */
    public function broadcastWith()
    {
        return [
            'count' => $this->favorites->getAllFavorites()
        ];
    }
}
