<?php

namespace DM\Events;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserWasCreated
 */
class UserWasCreated
{
    use SerializesModels;

    /**
     * @var $data
     */
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
}
