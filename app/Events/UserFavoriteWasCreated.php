<?php

namespace DM\Events;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserFavoriteWasCreated
 */
class UserFavoriteWasCreated
{
    use SerializesModels;

    /**
     * @var favId
     */
    public $favId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($favId)
    {
        $this->favId = $favId;
    }
}
