<?php
namespace DM\Helpers;

use DM\Models\V1\Category\Category;
use DM\Models\V1\Subcategory\Subcategory;
use DM\Models\V1\SubSubcategory\SubSubcategory;

/**
 * Website class
 */
class Website
{
    /**
     * @var $homeNavigationLinks
     */
    protected $homeNavigationLinks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->homeNavigationLinks = [
            'ooooo' => asset('user/', auth()->user()->id)
        ];
    }
    /**
     * Generate breadcrumbs categories list
     * home / category / subcategory / subsubcategory / ad
     */
    public static function getBreadcrumbs()
    {
        for ($i = 0; $i <= count(request()->segments()); $i++) {
            if ($i == 0) {
                $breadcrumbs[$i] = [
                   'breadcrumbs_segment' => 1,
                   'breadcrumbs_title' => trans('home.home'),
                   'breadcrumbs_url' => 'home'
             ];
           } elseif ($i == 1) {
                $breadcrumbs[$i] = [
                   'breadcrumbs_segment' => 1,
                   'breadcrumbs_title' => trans('home.ad.list'),
                   'breadcrumbs_url' => null
             ];
            } elseif ($i == 2) {
                $breadcrumbs[$i] = [
                   'breadcrumbs_segment' => 2,
                   'breadcrumbs_title' => Category::whereSlug(self::replaceUrl(request()->segments(), 2))
                      ->first()->name,
                   'breadcrumbs_url' => self::replaceUrl(request()->segments(), 2)
             ];
            } elseif ($i == 3) {
                $breadcrumbs[$i] = [
                   'breadcrumbs_segment' => 3,
                   'breadcrumbs_title' => Subcategory::whereSlug(self::replaceUrl(request()->segments(), 3))
                      ->first()->name,
                   'breadcrumbs_url' => self::replaceUrl(request()->segments(), 3)
             ];
            } elseif ($i == 4) {
                $breadcrumbs[$i] = [
                   'breadcrumbs_segment' => 4,
                   'breadcrumbs_title' => SubSubcategory::whereSlug(self::replaceUrl(request()->segments(), 4))
                      ->first()->name,
                   'breadcrumbs_url' => self::replaceUrl(request()->segments(), 4)
             ];
            } else {
                return 'Segments Fault!';
            }
        }

        return $breadcrumbs;
    }

    public static function replaceUrl($url, $segment)
    {
        return str_replace('skelbimai/', '', implode('/', array_slice($url, 0, $segment, true)));
    }
}
