<?php
namespace DM\Helpers;

/**
 * WebsiteNavigation class
 */
class WebsiteNavigation
{
    /**
     * Test
     *
     * @return mixed
     */
    public static function test()
    {
        return $this->isCurrentLoggedUser();
    }

    /**
     * Current User Id
     *
     * @return integer
     */
    public function isCurrentLoggedUser($userId = null)
    {
        // user id when logged in
        if (auth()->check()) {
            $userId = auth()->user()->id;
        }
        return $userId;
    }

}
