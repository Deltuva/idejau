<?php

namespace DM\Traits;

use DM\Models\V1\Category\Category;
use Request;

trait AdDataTrait
{
    public function scopeSearchByRequestParams($query)
    {
        $request = app()->make('request');
        $categoryId = 1;

        return $query
            ->orderBy($request->column ? $request->column : 'ad_id', $request->direction ? $request->direction : 'desc')
            ->orderBy('ad.ad_id', 'desc')
            // ->whereHas('adListingCategory', function ($query) use ($categoryId) {
            //   if ($categoryId) {
            //     $query->whereCatId($categoryId);
            //   }
            // })
            ->where(function ($query) use ($request) {
                if ($request->has('userType')) {
                    if ($request->userType == 1) {
                        $query->where('users.user_type', 1);
                    } elseif ($request->userType == 2) {
                        $query->where('users.user_type', 2);
                    }
                }
                if ($request->has('action')) {
                    if ($request->action == 1) {
                        $query->whereTypeId(1);
                    } elseif ($request->action == 2) {
                        $query->whereTypeId(2);
                    }
                }
            });
    }
}
