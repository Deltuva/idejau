<?php

namespace DM\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'DM\Events\UserWasCreated' => [
            'DM\Listeners\UserCreatedListener',
        ],
        'DM\Events\UserFavoriteWasCreated' => [
            'DM\Listeners\UserFavoriteCreatedListener',
        ],
        'DM\Events\UserUnFavoriteWasCreated' => [
            'DM\Listeners\UserUnFavoriteCreatedListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
