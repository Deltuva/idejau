<?php

namespace DM\Providers;

use Illuminate\Support\ServiceProvider;

//use Cache;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        $this->bindTransformers();
    }

    protected function bindTransformers()
    {
        //$this->app->bind('DM\Models\V1\Listing\Ad', 'DM\Transformers\AdListingTransformer');
        // $this->app->bind('Dingo\Api\Transformer\Adapter\Fractal', function($app) {
        //     $fractal = $app->make('\League\Fractal\Manager');
        //
        //     return new Fractal($fractal);
        // });
    }

    /**
     * Register any application services.
     */
    public function register()
    {
    }
}
