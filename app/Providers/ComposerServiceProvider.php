<?php

namespace DM\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composers([
            'DM\Http\ViewComposers\FavoriteComposer' => [
                '*'
            ],
            'DM\Http\ViewComposers\CategoryComposer' => [
                'frontend.pages.home'
            ]
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
