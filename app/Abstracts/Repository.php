<?php
namespace DM\Abstracts;

use DM\Repositories\V1\InterfaceMethods\RepositoryInterface as RepositoryInterface;

/**
 * The Abstract Repository provides default implementations of the methods defined
 * in the base repository interface. These simply delegate static function calls
 * to the right eloquent model based on the $modelClassName.
 */

abstract class Repository implements RepositoryInterface
{
    protected $modelClassName;

    /**
     * [create description]
     * @param  array  $attributes [description]
     * @return [type]             [description]
     */
    public function create(array $attributes)
    {
        return call_user_func_array("{$this->modelClassName}::create", array($attributes));
    }

    /**
     * [all description]
     * @param  array  $columns [description]
     * @return [type]          [description]
     */
    public function all($columns = array('*'))
    {
        return call_user_func_array("{$this->modelClassName}::all", array($columns));
    }

    /**
     * [find description]
     * @param  [type] $id      [description]
     * @param  array  $columns [description]
     * @return [type]          [description]
     */
    public function find($id, $columns = array('*'))
    {
        return call_user_func_array("{$this->modelClassName}::find", array($id, $columns));
    }

    /**
     * [destroy description]
     * @param  [type] $ids [description]
     * @return [type]      [description]
     */
    public function destroy($ids)
    {
        return call_user_func_array("{$this->modelClassName}::destroy", array($ids));
    }
}
