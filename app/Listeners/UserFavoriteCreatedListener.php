<?php

namespace DM\Listeners;

use DM\Events\UserFavoriteWasCreated;
use DM\Repositories\V1\FavoriteRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserFavoriteCreatedListener
{
    /**
     * @var FavoriteRepo
     */
    protected $favoriteRepo;

    /**
     * Constructor
     *
     * @param FavoriteRepository $favoriteRepo
     */
    public function __construct(FavoriteRepository $favoriteRepo)
    {
        $this->favoriteRepo = $favoriteRepo;
    }

    /**
     * Handle the event.
     *
     * @param  UserFavoriteWasCreated  $event
     * @return favorites[]
     */
    public function handle(UserFavoriteWasCreated $event)
    {
        $this->favoriteRepo->getCreateFavorited($event->favId);
        //\Log::info("Favorited id({$event->favId})");
    }
}
