<?php

namespace DM\Listeners;

use DM\Events\UserUnFavoriteWasCreated;
use DM\Repositories\V1\FavoriteRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserUnFavoriteCreatedListener
{
    /**
     * @var FavoriteRepo
     */
    protected $favoriteRepo;

    /**
     * Constructor
     *
     * @param FavoriteRepository $favoriteRepo
     */
    public function __construct(FavoriteRepository $favoriteRepo)
    {
        $this->favoriteRepo = $favoriteRepo;
    }

    /**
     * Handle the event.
     *
     * @param  UserUnFavoriteWasCreated  $event
     * @return favorites[]
     */
    public function handle(UserUnFavoriteWasCreated $event)
    {
        $this->favoriteRepo->getCreateUnFavorited($event->favId);
        //\Log::info("UnFavorited id({$event->favId})");
    }
}
