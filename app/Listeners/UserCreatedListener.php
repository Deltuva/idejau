<?php

namespace DM\Listeners;

use DM\Models\V1\User\User;
use DM\Events\UserWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasCreated  $event
     * @return void
     */
    public function handle(UserWasCreated $event)
    {
        return User::create([
            'name' => '-',
            'email' => $event->data['email'],
            'user_type' => $event->data['user_registration']['user_type'] == 1 ? 'private' : 'company',
            'subscribe' => $event->data['user_registration']['subscribe'] == 1 ? true : false,
            'terms_agree' => $event->data['user_registration']['terms_agree'] == 1 ? true : false,
            'password'=> bcrypt($event->data['password']),
        ]);
    }
}
