<?php

namespace DM\Transformers;

use League\Fractal\TransformerAbstract;
use DM\Models\v1\User\User;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'name' => $user->name,
            'email' => $user->email,
            'added' => date('Y-m-d', strtotime($user->created_at))
        ];
    }
}
