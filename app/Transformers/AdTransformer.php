<?php

namespace DM\Transformers;

use League\Fractal\TransformerAbstract;
use DM\Models\V1\Ad\Ad;

class AdTransformer extends TransformerAbstract
{
    public function transform(Ad $adItem)
    {
        return [
          'id' => $adItem->ad_id,
          'utype' => $adItem->user_type,
          'company' => ['image' => $adItem->present()->adCompanyImage, 'name' => $adItem->present()->adCompanyName],
          'widget' => ['new' => $adItem->present()->todayAdWidget],
          'title' => $adItem->present()->adTitleName,
          'pic' => ['image' => $adItem->present()->adImage,'total_images' => $adItem->present()->adImageCount],
          'description' => $adItem->ad_description,
          'price' => $adItem->ad_price,
          'location' => $adItem->present()->locationName,
          'ago_created' => $adItem->present()->timeAgoDays,
          'ago_edit' => $adItem->present()->timeAgoEditedStatus,
          'more' => $adItem->present()->viewByCategory
        ];
    }
}
