<?php

namespace DM\Http\Middleware;

use Closure;
use DM\Models\V1\Category\Category;
use DM\Models\V1\Subcategory\Subcategory;
use DM\Models\V1\SubSubcategory\SubSubcategory;

class RedirectIfNotHasCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
         try {
             if (isset($request->category_slug)) {
                 Category::where('slug', $request->category_slug)
                     ->firstOrFail();
             }
             if (isset($request->subcategory_slug)) {
                 Subcategory::where('slug', $request->subcategory_slug)
                     ->firstOrFail();
             }
             if (isset($request->subsubcategory_slug)) {
                 Subsubcategory::where('slug', $request->subsubcategory_slug)
                     ->firstOrFail();
             }
         } catch (\Exception $e) {
             return redirect()->to('/');
         }

         return $next($request);
     }
}
