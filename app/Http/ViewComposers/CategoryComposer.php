<?php

namespace DM\Http\ViewComposers;

use Illuminate\View\View;
use DM\Models\V1\Category\Category;
use Cache;

class CategoryComposer
{
    /**
     * @var $category
     */
    protected $category;

    /**
     * Constructor
     */
    public function __construct(Category $category)
    {
        $this->categories = $category;
    }

    /**
     * Get Home Categories
     * 
     * @param  View  $view
     */
    public function compose(View $view)
    {   
        $categories = Cache::rememberForever('categories', function () {
            return $this->categories->with('subcategories')
               ->get();
        });
        $view->with('categories', $categories);
    }
}