<?php

namespace DM\Http\ViewComposers;

use Illuminate\View\View;
use DM\Repositories\V1\FavoriteRepository;

class FavoriteComposer
{
    /**
     * @var FavoriteRepo
     */
    protected $favoriteRepo;

    /**
     * Constructor
     */
    public function __construct(FavoriteRepository $favoritesRepo)
    {
        $this->favoritesRepo = $favoritesRepo;
    }

    /**
     * Get User Favorites
     * 
     * @param  View  $view
     */
    public function compose(View $view)
    {   
        $countFavorites = $this->favoritesRepo->getCountAllFavorites();
        $view->with('countUserFavorites', $countFavorites);

        $favorites = $this->favoritesRepo->getAllFavorites();
        $view->with('userFavorites', $favorites);
    }

    /**
     * Current User Id
     *
     * @return integer
     */
    public function isCurrentLoggedUser($userId = null)
    {
        // user id when logged in
        if (auth()->check()) {
            $userId = auth()->user()->id;
        }
        return $userId;
    }
}