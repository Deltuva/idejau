<?php

namespace DM\Http\Controllers\Auth;

use DM\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Display login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        if (view()->exists('frontend.auth.login')) {
            return view('frontend.auth.login');
        }
    }
}
