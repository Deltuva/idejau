<?php

namespace DM\Http\Controllers\Auth;

use DM\Models\V1\User\User;
use DM\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo                 = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display register form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (view()->exists('frontend.auth.register')) {
            return view('frontend.auth.register');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_type' => 'required',
            'email'                       => 'required|email|max:255|unique:users',
            'password'                    => 'required|min:6|confirmed',
            'password_confirmation'       => 'required|min:6'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'                           => '-',
            'email'                          => $data['email'],
            'user_type'                      => isset($data['user_type']) == 1 ? 'private' : 'company',
            'is_subscribe'                   => isset($data['subscribe']) == 1 ? true : false,
            'is_terms_agree'                 => isset($data['terms_agree']) == 1 ? true : false,
            'password'                       => bcrypt($data['password']),
        ]);
    }
}
