<?php

namespace DM\Http\Controllers\Backend;

use DM\Http\Controllers\Controller;

class AdminController extends Controller
{
    protected $backDir = 'backend/';

    /**
     * Admin page
     *
     * @return \Illuminate\Http\Response
     */
    public function backHome()
    {
        $message = 'Backend...';

        return view($this->backDir. 'pages.index', [
            'backend' => $message
        ]);
    }
}
