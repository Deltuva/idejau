<?php

namespace DM\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use DM\Http\Controllers\Controller;
use DM\Repositories\V1\CategoryRepository;
use DM\Repositories\V1\SubcategoryRepository;
use DM\Repositories\V1\SubSubcategoryRepository;
use DM\Repositories\V1\AdRepository;
// use DM\Transformers\AdTransformer;
// use Dingo\Api\Routing\Helpers;
use DM\Sanitize\CleanInput;
use DM\Events\UserFavorite;

class AdController extends Controller
{
    /**
     * @var $categoryRepo
     */
    protected $categoryRepo;

    /**
     * @var $subcategoryRepo
     */
    protected $subcategoryRepo;

    /**
     * @var $adRepo
     */
    protected $adRepo;

    /**
     * @var $frontDir
     */
    protected $frontDir = 'frontend/';

    /**
     * [Controller constructor]
     *
     * @param CategoryRepository $categoryRepo
     * @param SubcategoryRepository $subcategoryRepo
     * @param AdRepository $adRepo
     */
    public function __construct(
      CategoryRepository $categoryRepo,
      SubcategoryRepository $subcategoryRepo,
      SubSubcategoryRepository $subsubcategoryRepo,
      AdRepository $adRepo)
    {
        $this->categoryRepo = $categoryRepo;
        $this->subcategoryRepo = $subcategoryRepo;
        $this->subsubcategoryRepo = $subsubcategoryRepo;
        $this->adRepo = $adRepo;
    }

    /**
     * Ad page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        //dd($request->get('search')['in']);

        return view($this->frontDir. 'ad.list', [
          'adcategorieslist'     => $this->categoryRepo->getCategories(),
          'adhead'               => $this->adRepo->getAllPublicList()->count(),
          'adhead_private_count' => $this->adRepo->getCountPrivateUsers(),
          'adhead_company_count' => $this->adRepo->getCountCompanyUsers(),
          'adlist'               => $this->adRepo->getAllPublicList()
        ]);
    }

    /**
     * Ad page on by category
     *
     * @param Request $request
     * @param category $c1
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexCat(Request $request, $slug = null)
    {
        $adcatInfo = $this->categoryRepo->findIdBySlug(CleanInput::cleaner($slug));

        return view($this->frontDir. 'ad.list', [
          'adsubcategorieslist' => $this->subcategoryRepo->getSubcategories($adcatInfo->id),
          'adhead'              => $this->categoryRepo->getAdTitle($adcatInfo->id),
          'adlist'              => $this->adRepo->getAllPublicList(['cat' => CleanInput::cleaner($adcatInfo->id, 'num')])
        ]);
    }

    /**
     * Ad page on by subcategory
     *
     * @param Request $request
     * @param category $c1
     * @param subcategory $c2
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexSub(Request $request, $c1 = null, $c2 = null)
    {
        $slug = $c1.'/'.$c2;
        $adsubInfo = $this->subcategoryRepo->findIdBySlug(CleanInput::cleaner($slug));

        return view($this->frontDir. 'ad.list', [
          'adsubsubcategorieslist' => $this->subsubcategoryRepo->getSubSubcategories($adsubInfo->id),
          'adhead'                 => $this->subcategoryRepo->getAdTitle($adsubInfo->id),
          'adlist'                 => $this->adRepo->getAllPublicList(['sub' => CleanInput::cleaner($adsubInfo->id, 'num')])
        ]);
    }

    /**
     * Ad page on by subsubcategory
     *
     * @param Request $request
     * @param category $c1
     * @param subcategory $c2
     * @param subsubcategory $c3
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexSubsub(Request $request, $c1 = null, $c2 = null, $c3 = null)
    {
        $slug = $c1.'/'.$c2.'/'.$c3;
        $adsubsubInfo = $this->subsubcategoryRepo->findIdBySlug(CleanInput::cleaner($slug));

        return view($this->frontDir. 'ad.list', [
          'adhead' => $this->subsubcategoryRepo->getAdTitle($adsubsubInfo->id),
          'adlist' => $this->adRepo->getAllPublicList(['subsub' => CleanInput::cleaner($adsubsubInfo->id, 'num')])
        ]);
    }

    /**
     * Show the application ad listing.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdListingIndex()
    {
        $adlist = $this->adRepo->getAllPublicList();
        return $this->response->paginator($adlist, new AdTransformer)
           ->setMeta($this->newPaginateMeta($adlist));
    }

    /**
     * Favorite
     *
     * @return Response
     */
    public function favoriteNewAd($favId)
    {
        $this->isCurrentAuthUser()->favorites()
          ->syncWithoutDetaching([$favId]);

        return back();
    }

    /**
     * Unfavorite
     *
     * @return Response
     */
    public function unfavoriteNewAd($favId)
    {
        $this->isCurrentAuthUser()->favorites()
          ->detach([$favId]);

        return back();
    }

    /**
     * Current Auth User
     *
     * @return object
     */
    public function isCurrentAuthUser($user = null)
    {
        // user object when logged in
        if (auth()->check()) {
            $user = auth()->user();
        }
        return $user;
    }
}
