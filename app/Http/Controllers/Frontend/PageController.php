<?php

namespace DM\Http\Controllers\Frontend;

use DM\Http\Controllers\Controller;

class PageController extends Controller
{
    protected $frontDir = 'frontend/';

    /**
     * Home page
     *
     * @return \Illuminate\Http\Response
     */
    public function pageHome()
    {
        return view($this->frontDir. 'pages.home');
    }

    /**
     * Term page
     *
     * @return \Illuminate\Http\Response
     */
    public function pageTerm()
    {
        return view($this->frontDir. 'pages.term');
    }

    /**
     * About page
     *
     * @return \Illuminate\Http\Response
     */
    public function pageAbout()
    {
        return view($this->frontDir. 'pages.about');
    }

    /**
     * DUK page
     *
     * @return \Illuminate\Http\Response
     */
    public function pageDuk()
    {
        return view($this->frontDir. 'pages.duk');
    }

    /**
     * Developer page
     *
     * @return \Illuminate\Http\Response
     */
    public function pageDeveloper()
    {
        return view($this->frontDir. 'pages.developer');
    }

    /**
     * User favorites page
     *
     * @return \Illuminate\Http\Response
     */
    public function pageUserFavorites()
    {
        return view($this->frontDir. 'user.favorites');
    }

}
