<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarEngine\CarEngine;

class CAR_EngineController extends VueController
{
    /**
     * Get Car Engine Collection
     * @param  CarEngine $carEngineModel
     * @return json|array
     */
    public function getCollection(CarEngine $carEngineModel)
    {
        try {
            $responseEnginesCollection = [];
            $statusCode = $this->setStatusCode(200);
            $enginesCollection = $carEngineModel->select('car_engine_id', 'car_engine_name')->orderBy('car_engine_id', 'asc')->get();
                foreach ($enginesCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseEnginesCollection[$key] = [
                          'id' => $collectionItem->car_engine_id,
                          'engine' => $collectionItem->car_engine_name,
                          'selected' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseEnginesCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
