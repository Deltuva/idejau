<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarBody\CarBody;

class CAR_BodyController extends VueController
{
    /**
     * Get Car Body Collection
     * @param  CarBody $carBodyModel
     * @return json|array
     */
    public function getCollection(CarBody $carBodyModel)
    {
        try {
            $responseBodysCollection = [];
            $statusCode = $this->setStatusCode(200);
            $bodysCollection = $carBodyModel->select('car_body_id', 'car_body_name')->orderBy('car_body_id', 'asc')->get();
                foreach ($bodysCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseBodysCollection[$key] = [
                          'id' => $collectionItem->car_body_id,
                          'body' => $collectionItem->car_body_name,
                          'checked' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseBodysCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
