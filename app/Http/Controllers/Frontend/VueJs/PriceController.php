<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\Price\Price;

class PriceController extends VueController
{
    /**
     * Get Price Collection
     * @param  Price $priceModel
     * @return json|array
     */
    public function getCollection(Price $priceModel)
    {
        try {
            $responsePricesCollection = [];
            $statusCode = $this->setStatusCode(200);
            $pricesCollection = $priceModel->select('price_id', 'price_name')->orderBy('price_id', 'asc')->get();
            foreach ($pricesCollection as $key => $collectionItem) {
                if (!is_null($collectionItem)) {
                    $responsePricesCollection[$key] = [
                          'id' => $collectionItem->price_id,
                          'price' => $collectionItem->price_name,
                          'selected' => false
                        ];
                }
            }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responsePricesCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
