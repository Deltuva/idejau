<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarMake\CarMake;

class CAR_MakeController extends VueController
{
    /**
     * Get Car Make Names Collection
     * @param  CarMake $carMakeModel
     * @return json|array
     */
    public function getCollection(CarMake $carMakeModel)
    {
        try {
            $responseMakesCollection = [];
            $statusCode = $this->setStatusCode(200);
            $makesCollection = $carMakeModel->select('car_make_id', 'car_make_name')->whereCarMakeActive(true)->orderBy('car_make_id', 'asc')->get();
                foreach ($makesCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseMakesCollection[$key] = [
                          'id' => $collectionItem->car_make_id,
                          'make' => $collectionItem->car_make_name,
                          'checked' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseMakesCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
