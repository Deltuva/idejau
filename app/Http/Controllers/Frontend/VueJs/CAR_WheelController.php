<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarWheel\CarWheel;

class CAR_WheelController extends VueController
{
    /**
     * Get Car Wheel Collection
     * @param  CarWheel $carWheelModel
     * @return json|array
     */
    public function getCollection(CarWheel $carWheelModel)
    {
        try {
            $responseWheelsCollection = [];
            $statusCode = $this->setStatusCode(200);
            $wheelsCollection = $carWheelModel->select('car_wheel_id', 'car_wheel_name')->orderBy('car_wheel_id', 'asc')->get();
                foreach ($wheelsCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseWheelsCollection[$key] = [
                          'id' => $collectionItem->car_wheel_id,
                          'wheel' => $collectionItem->car_wheel_name,
                          'checked' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseWheelsCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
