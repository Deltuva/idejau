<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use Illuminate\Http\Request;
use DM\Events\UserFavoriteWasCreated;
use DM\Events\UserUnFavoriteWasCreated;

class FavoriteController extends VueController
{
    /**
     * Post new favorite
     * @param  $favId
     * @return json|status
     */
    public function favoriteNewAd($favId)
    {
        try {
            $statusCode = $this->setStatusCode(200);
            event(new UserFavoriteWasCreated($favId));

        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return back();
        }
    }

    /**
     * Post new favorite
     * @param  $favId
     * @return json|status
     */
    public function unfavoriteNewAd($favId)
    {
        try {
            $statusCode = $this->setStatusCode(200);
            event(new UserUnFavoriteWasCreated($favId));
            
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return back();
        }
    }

    /**
     * Count user favorites
     * 
     * @return mixed
     */
    public function countAllUserFavorites()
    {
        try {
            $statusCode = $this->setStatusCode(200);
            $countFavorites = $this->isCurrentAuthUser()->favorites()
               ->count();

        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()->json(['data' => $countFavorites, 'status' => $this->getStatusCode()]);
        }
    }

    /**
     * Current Auth User
     *
     * @return object
     */
    public function isCurrentAuthUser($user = null)
    {
        // user object when logged in
        if (auth()->check()) {
            $user = auth()->user();
        }
        return $user;
    }
}
