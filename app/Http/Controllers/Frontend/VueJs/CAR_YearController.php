<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarYear\CarYear;

class CAR_YearController extends VueController
{
    /**
     * Get Car Year Collection
     * @param  CarYear $carYearModel
     * @return json|array
     */
    public function getCollection(CarYear $carYearModel)
    {
        try {
            $responseYearsCollection = [];
            $statusCode = $this->setStatusCode(200);
            $yearsCollection = $carYearModel->select('car_year_id', 'car_year_name')->orderBy('car_year_id', 'asc')->get();
                foreach ($yearsCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseYearsCollection[$key] = [
                          'id' => $collectionItem->car_year_id,
                          'year' => $collectionItem->car_year_name,
                          'selected' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseYearsCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
