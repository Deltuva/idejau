<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarDamaged\CarDamaged;

class CAR_DamageController extends VueController
{
    /**
     * Get Car Damaged Collection
     * @param  CarDamaged $carDamagedModel
     * @return json|array
     */
    public function getCollection(CarDamaged $carDamageModel)
    {
        try {
            $responseDamagesCollection = [];
            $statusCode = $this->setStatusCode(200);
            $damagesCollection = $carDamageModel->select('car_damage_id', 'car_damage_name')->orderBy('car_damage_id', 'asc')->get();
                foreach ($damagesCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseDamagesCollection[$key] = [
                          'id' => $collectionItem->car_damage_id,
                          'damage' => $collectionItem->car_damage_name,
                          'selected' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseDamagesCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
