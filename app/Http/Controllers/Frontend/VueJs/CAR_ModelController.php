<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarModel\CarModel;

class CAR_ModelController extends VueController
{
    /**
     * Get Car Make by Model Names Collection
     * @param  CarMake $carMakeModel
     * @return json|array
     */
    public function getCollection(CarModel $carModel, $makeId)
    {
        try {
            $responseModelsCollection = [];
            $statusCode = $this->setStatusCode(200);
            $makeCollectionIds = explode(',', $makeId);
            $modelsCollection = $carModel->select('car_model_id', 'car_make_id', 'car_model_name')
                ->whereCarModelActive(true)
                ->whereIn('car_make_id', $makeCollectionIds)
                ->groupBy('car_model_name')
                ->orderBy('car_model_id', 'asc')->get();
            foreach ($modelsCollection as $key => $collectionItem) {
                if (!is_null($collectionItem)) {
                    $responseModelsCollection[$key] = [
                        'id' => $collectionItem->car_model_id,
                        'make' => $collectionItem->carSingleMake->car_make_name,
                        'model' => $collectionItem->car_model_name,
                        'checked' => false
                    ];
                }
            }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseModelsCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
