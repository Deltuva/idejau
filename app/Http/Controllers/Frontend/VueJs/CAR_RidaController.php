<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarRida\CarRida;

class CAR_RidaController extends VueController
{
    /**
     * Get Car Rida Collection
     * @param  CarRida $carRidaModel
     * @return json|array
     */
    public function getCollection(CarRida $carRidaModel)
    {
        try {
            $responseRidasCollection = [];
            $statusCode = $this->setStatusCode(200);
            $ridasCollection = $carRidaModel->select('car_rida_id', 'car_rida_value')->orderBy('car_rida_id', 'asc')->get();
                foreach ($ridasCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseRidasCollection[$key] = [
                          'id' => $collectionItem->car_rida_id,
                          'rida' => $collectionItem->car_rida_value,
                          'selected' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseRidasCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
