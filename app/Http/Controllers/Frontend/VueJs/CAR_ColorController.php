<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarColor\CarColor;

class CAR_ColorController extends VueController
{
    /**
     * Get Car Color Collection
     * @param  CarColor $carColorModel
     * @return json|array
     */
    public function getCollection(CarColor $carColorModel)
    {
        try {
            $responseColorsCollection = [];
            $statusCode = $this->setStatusCode(200);
            $colorsCollection = $carColorModel->select('car_color_id', 'car_color_name')->orderBy('car_color_id', 'asc')->get();
                foreach ($colorsCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseColorsCollection[$key] = [
                          'id' => $collectionItem->car_color_id,
                          'color' => $collectionItem->car_color_name,
                          'selected' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseColorsCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
