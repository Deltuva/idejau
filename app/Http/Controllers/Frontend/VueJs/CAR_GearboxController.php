<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarGearbox\CarGearbox;

class CAR_GearboxController extends VueController
{
    /**
     * Get Car Gearbox Collection
     * @param  CarGearbox $carGearboxModel
     * @return json|array
     */
    public function getCollection(CarGearbox $carGearboxModel)
    {
        try {
            $responseGearboxesCollection = [];
            $statusCode = $this->setStatusCode(200);
            $gearboxesCollection = $carGearboxModel->select('car_gearbox_id', 'car_gearbox_name')->orderBy('car_gearbox_id', 'desc')->get();
                foreach ($gearboxesCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseGearboxesCollection[$key] = [
                          'id' => $collectionItem->car_gearbox_id,
                          'gearbox' => $collectionItem->car_gearbox_name,
                          'checked' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseGearboxesCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
