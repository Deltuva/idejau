<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarPower\CarPower;

class CAR_PowerController extends VueController
{
    /**
     * Get Car Power Collection
     * @param  CarMake $carMakeModel
     * @return json|array
     */
    public function getCollection(CarPower $carPowerModel)
    {
        try {
            $responsePowersCollection = [];
            $statusCode = $this->setStatusCode(200);
            $powersCollection = $carPowerModel->select('car_power_id', 'car_power_value')->orderBy('car_power_id', 'asc')->get();
                foreach ($powersCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responsePowersCollection[$key] = [
                          'id' => $collectionItem->car_power_id,
                          'power' => $collectionItem->car_power_value,
                          'selected' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responsePowersCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
