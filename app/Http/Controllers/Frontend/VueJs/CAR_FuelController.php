<?php

namespace DM\Http\Controllers\Frontend\VueJs;

use DM\Http\Controllers\Frontend\VueController;
use DM\Models\V1\CarFuel\CarFuel;

class CAR_FuelController extends VueController
{
    /**
     * Get Car Fuel Collection
     * @param  CarFuel $carFuelModel
     * @return json|array
     */
    public function getCollection(CarFuel $carFuelModel)
    {
        try {
            $responseFuelsCollection = [];
            $statusCode = $this->setStatusCode(200);
            $fuelsCollection = $carFuelModel->select('car_fuel_id', 'car_fuel_name')->orderBy('car_fuel_id', 'asc')->get();
                foreach ($fuelsCollection as $key => $collectionItem) {
                    if (!is_null($collectionItem)) {
                        $responseFuelsCollection[$key] = [
                          'id' => $collectionItem->car_fuel_id,
                          'fuel' => $collectionItem->car_fuel_name,
                          'checked' => false
                        ];
                    }
                }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
               ->json(['data' => $responseFuelsCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
