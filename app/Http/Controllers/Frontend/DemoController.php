<?php

namespace DM\Http\Controllers\Frontend;

use DM\Http\Controllers\Controller;
use DM\Helpers\Website;

class DemoController extends Controller
{
    public function index()
    {
        return dd(Website::getBreadcrumbs());
    }

    public function categories($categories)
    {
        $categoriesTreeCollection = explode('/', $categories);
        if ($categoriesTreeCollection) {
            $postSlug = $categoriesTreeCollection;
            foreach ($categoriesTreeCollection as $k => $v) {
                echo $v.'<br/>';
            }
        }

        //dd($postSlug);
        //      ->where('categories','^[a-zA-Z0-9-_\/]+$');
       // here you can manage the categories in $categories array and slug of the post in $postSlug
    }
}
