<?php

namespace DM\Presenters;

use Laracasts\Presenter\Presenter;
use DM\Constants\PageInterface;
use Carbon\Carbon;

/**
 * Class AdPresenter.
 */
class AdPresenter extends Presenter implements PageInterface
{
    /**
     * Display image file.
     *
     * @return image
     */
    public function adImage()
    {
        if ($this->adPictureAt) {
            if ($this->adPictureHideAt == false) {
                return asset('images/hide-image.jpg');
            } else {
                return url('images/photos/'.$this->adPictureAt);
            }
        } else {
            return asset('images/no-image.jpg');
        }
    }

    /**
     * Display company image file.
     *
     * @return image
     */
    public function adCompanyImage()
    {
        if (!is_null($this->companyImage)) {
            //return asset('images/companies/muuray.png');
            return url('images/companies/'.$this->companyImage);
        }
    }

    /**
     * Display company name.
     *
     * @return string
     */
    public function adCompanyName()
    {
        if (!is_null($this->companyName) || !is_null($this->companyType)) {
            return $this->companyType.' '.$this->companyName;
        }
    }

    /**
     * Total Pics.
     *
     * @return mixed
     */
    public function adImageCount()
    {
        if ($this->adPictureCountAt > 1) {
            return '<i class="fa fa-camera"></i> '.$this->adPictureCountAt;
        }
    }

    /**
     * Display Title Name.
     *
     * @return string
     */
    public function adTitleName()
    {
        if (!is_null($this->carSingleMake->car_make_name) || !is_null($this->carSingleBody->car_body_name) || !is_null($this->carSingleEngine->car_engine_name)) {
            return $this->carSingleMake->car_make_name.' '.@$this->carSingleModel->car_model_name.', '.$this->carSingleEngine->car_engine_name.' l, '.$this->carSingleBody->car_body_name;
        }
    }

    /**
     * Description cut "...".
     *
     * @return string
     */
    public function limitDescription()
    {
        if (!is_null($this->ad_description)) {
            return str_limit($this->ad_description, AdPresenter::AD_DESCRIPTION_LIMIT);
        }
    }

    /**
     * Display Ad detail owned by category
     *
     * @return string
     */
    public function viewByCategory()
    {
        if ($this->detail_id > 0) {
            //cars category
            if ($this->detail_id === 1) {
                if (!is_null($this->carSingleYear->car_year_name)) {
                    $car_year = $this->carSingleYear->car_year_name.'-';
                }
                if (!is_null($this->carSingleMonth->car_month_name)) {
                    $car_month = $this->carSingleMonth->car_month_name.'m.<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleFuel->car_fuel_name)) {
                    $car_fuel = $this->carSingleFuel->car_fuel_name.'<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleEngine->car_engine_name)) {
                    $car_engine = $this->carSingleEngine->car_engine_name.' l<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleGearbox->car_gearbox_name)) {
                    $car_gearbox = $this->carSingleGearbox->car_gearbox_name.'<span class="dec_space">|</span>';
                }
                if (!is_null($this->car_km)) {
                    $car_rida = $this->car_km.' km<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleDoor->car_door_name)) {
                    $car_door = $this->carSingleDoor->car_door_name;
                }
                return $car_year.$car_month.$car_fuel.$car_engine.$car_gearbox.$car_rida.$car_door;
            }
        }
    }

    /**
     * Display lastname.
     *
     * @return string
     */
    public function latestName()
    {
        if (!is_null($this->ad_name)) {
            return str_limit($this->ad_name, 30);
        }
    }

    /**
     * Display City Name.
     *
     * @return string
     */
    public function locationName()
    {
        if (!is_null($this->cityName)) {
            return $this->cityName;
        }
    }

    /**
     * Display User Type.
     *
     * @return string
     */
    public function userTypeName()
    {
        if (!is_null($this->user_type)) {
            if ($this->user_type == 'private') {
                // return trans('home.ad.user_type_private');
            } elseif ($this->user_type == 'company') {
                return trans('home.ad.user_type_company');
            }
        }
    }

    /**
     * Display latest view status.
     *
     * @return true
     */
    public function latestVisitStatus()
    {
        $lastViewAd = [
          'post_id' => $this->id,
        ];

        if (session()->has('last_view')) {
            $lastViewArray = session('last_view');

            foreach ($lastViewArray as $k => $v) {
                if ($v['post_id'] == $lastViewAd['post_id']) {
                    $this->visited = true;
                    break;
                }
            }
        }

        return $this->visited;
    }

    /**
     * Display html stars counter widget.
     */
    public function starsCountWidget()
    {
        $iconWidget = '<div class="star-wrap">';
        $iconWidget .= '<div class="star-i">';
        $iconWidget .= '<i class="fa fa-star" aria-hidden="true"></i>'.$this->starTotal;
        $iconWidget .= '</div>';
        $iconWidget .= '</div>';

        return $this->starTotal ? $iconWidget : '';
    }

    /**
     * Display carbon time today.
     *
     * @return mixed
     */
    public function todayAdWidget()
    {
        $createdToday = new Carbon($this->created_at, 'Europe/Vilnius');
        $now = Carbon::now();

        $iconWidget = '<div class="new">';
        $iconWidget .= '<div class="icon">';
        $iconWidget .= '</div>';
        $iconWidget .= '</div>';

        $todayStatus = ($createdToday->diff($now)->days < AdPresenter::AD_DIFF_IN_DAYS) ? $iconWidget : null;

        return $todayStatus;
    }

    /**
     * Display carbon time ago in days.
     *
     * @return mixed
     */
    public function timeAgoDays()
    {
        setlocale(LC_TIME, 'Lithuania');
        Carbon::setLocale('lt');

        $createdDate = new Carbon($this->created_at, 'Europe/Vilnius');
        //$now = Carbon::now();
        $date = Carbon::createFromTimeStamp(strtotime($createdDate))->diffForHumans();

        if ($createdDate->diffInDays() < AdPresenter::AD_DIFF_IN_DAYS) {
            $agoTime = $createdDate->diffForHumans();
            $replacedAgoTime = ucfirst(str_replace(['valandą', 'valandas', 'valandų'], 'val.', $agoTime));
            $iconWidget = '<span class="time today">';
            $iconWidget .= '<i class="fa fa-clock-o" aria-hidden="true"></i> '.$replacedAgoTime;
            $iconWidget .= '</span>';
        } else {
            $agoTime = 'Prieš <b>'.$createdDate->diffInDays().'</b> d.';
            $iconWidget = '<span class="time">';
            $iconWidget .= $agoTime;
            $iconWidget .= '</span>';
        }

        $agoStatus = $iconWidget;

        return $agoStatus;
    }

    /**
     * Display carbon time ago in minutes status message.
     *
     * @return string
     */
    public function timeAgoEditedStatus()
    {
        $updatedDate = new Carbon($this->updated_at, 'Europe/Vilnius');

        //$now = Carbon::now('Europe/Vilnius');
        if ($updatedDate->diffInMinutes() <= AdPresenter::AD_DIFF_IN_MINUTES) { //5 minutes
            $iconWidget = '<span class="time edited">';
            $iconWidget .= '<i class="fa fa-pencil-square" aria-hidden="true"></i> ';
            $iconWidget .= trans('home.ad.updated_status');
            $iconWidget .= '</span>';
        } else {
            $iconWidget = '';
        }

        $agoStatus = $iconWidget;

        return $agoStatus;
    }
}
