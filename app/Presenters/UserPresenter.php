<?php

namespace DM\Presenters;

use Laracasts\Presenter\Presenter;

/**
 * Class UserPresenter.
 */
class UserPresenter extends Presenter
{
    /**
     * Display image file.
     *
     * @return string
     */
    public function fullUserCompanyName()
    {
        //if (!is_null($this->post_description)) {
            return $this->first()->name;
        //}
    }
}
