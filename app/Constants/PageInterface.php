<?php

namespace DM\Constants;

interface PageInterface
{
    //ads
    const AD_DESCRIPTION_LIMIT = 100;
    const AD_DIFF_IN_DAYS = 1;
    const AD_DIFF_IN_MINUTES = 5;
    const AD_ITEMS_PER_PAGE = 10;
}
