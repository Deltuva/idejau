<?php

namespace DM\Models\V1\AdOption;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'ad_options';
    protected $primaryKey = 'ad_option_id';
    protected $fillable = ['ad_id', 'option_id'];
    public $timestamps = false;
}
