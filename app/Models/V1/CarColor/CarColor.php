<?php

namespace DM\Models\V1\CarColor;

use Illuminate\Database\Eloquent\Model;

class CarColor extends Model
{
    protected $table = 'car_colors';
    protected $primaryKey = 'car_color_id';
    protected $fillable = ['car_color_name'];
    public $timestamps = false;
}
