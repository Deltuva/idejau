<?php

namespace DM\Models\V1\Municipality;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $table = 'municipalitys';
    protected $primaryKey = 'municipality_id';
    protected $fillable = ['municipality_name'];
    public $timestamps = false;
}
