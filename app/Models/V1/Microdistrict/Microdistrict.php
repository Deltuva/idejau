<?php

namespace DM\Models\V1\Microdistrict;

use Illuminate\Database\Eloquent\Model;

class Microdistrict extends Model
{
    protected $table = 'microdistricts';
    protected $primaryKey = 'microdistrict_id';
    protected $fillable = ['place_id', 'microdistrict_name'];
    public $timestamps = false;
}
