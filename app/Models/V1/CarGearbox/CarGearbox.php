<?php

namespace DM\Models\V1\CarGearbox;

use Illuminate\Database\Eloquent\Model;

class CarGearbox extends Model
{
    protected $table = 'car_gearboxs';
    protected $primaryKey = 'car_gearbox_id';
    protected $fillable = ['car_gearbox_name'];
    public $timestamps = false;
}
