<?php

namespace DM\Models\V1\CarDamaged;

use Illuminate\Database\Eloquent\Model;

class CarDamaged extends Model
{
    protected $table = 'car_damageds';
    protected $primaryKey = 'car_damaged_id';
    protected $fillable = ['car_damaged_name'];
    public $timestamps = false;
}
