<?php

namespace DM\Models\V1\CarBody;

use Illuminate\Database\Eloquent\Model;

class CarBody extends Model
{
    protected $table = 'car_bodys';
    protected $primaryKey = 'car_body_id';
    protected $fillable = ['car_body_name'];
    public $timestamps = false;
}
