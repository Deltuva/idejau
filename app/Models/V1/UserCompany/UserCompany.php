<?php

namespace DM\Models\V1\UserCompany;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    protected $table = 'users_companies';
    protected $primaryKey = 'company_id';
    public $timestamps = false;
}
