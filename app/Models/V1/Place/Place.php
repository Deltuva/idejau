<?php

namespace DM\Models\V1\Place;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'places';
    protected $primaryKey = 'place_id';
    protected $fillable = ['municipality_id', 'place_name'];
    public $timestamps = false;
}
