<?php

namespace DM\Models\V1\User;

use Auth;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    const TYPE_USER_PRIVATE = 'private';
    const TYPE_USER_COMPANY = 'company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'user_type', 'is_terms_agree', 'is_subscribe', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * User Id.
     *
     * @return id
     */
    public function getUserId()
    {
        return $this->id;
    }

    /**
     * Get all of favorite ad for the user.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function favorites()
    {
        return $this->belongsToMany('DM\Models\V1\Ad\Ad', 'users_favorites', 'user_id', 'ad_id')
           ->withTimeStamps();
    }

    /**
     * User Company Relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function getUserCompany()
    {
        return $this->hasOne('DM\Models\V1\UserCompany\UserCompany')
          ->first();
    }

    /**
     * User Company Relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function getUserPrivate()
    {
        return $this->hasOne('DM\Models\V1\UserPrivate\UserPrivate')
          ->first();
    }

    /**
     * Auth status.
     *
     * @return true/false
     */
    public function isLoginUser()
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Full display name.
     *
     * @return string
     */
    public function getFullUserName()
    {
        if ($this->isLoginUser() == true) {
            if ($this->user_type == self::TYPE_USER_PRIVATE) {
                $privatesRelation = $this->getUserPrivate();

                return $privatesRelation ? $privatesRelation
                   ->email : $this->email;
            } elseif ($this->user_type == self::TYPE_USER_COMPANY) {
                $companiesRelation = $this->getUserCompany();

                return $companiesRelation ? $companiesRelation
                     ->name.', '.$companiesRelation
                          ->type : $this->getUserName().'('.$this->getUserEmail().')';
            }
        }
    }

    /**
     * User Name.
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->name;
    }

    /**
     * User Email.
     *
     * @return string
     */
    public function getUserEmail()
    {
        return $this->email;
    }
}
