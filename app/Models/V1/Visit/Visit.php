<?php

namespace DM\Models\V1;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table = 'visits';
    protected $primaryKey = 'visit_id';
    protected $fillable = ['uid', 'pslug' ,'device', 'ip'];
    public $timestamps = true;
}
