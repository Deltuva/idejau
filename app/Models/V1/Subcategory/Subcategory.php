<?php

namespace DM\Models\V1\Subcategory;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'subcategories';

    public $timestamps = false;

    /**
     * An Subcategory is owned by a post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('DM\Models\V1\Post', 'cid');
    }

    /**
     * An Subcategory is owned by a ad category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listingCategories()
    {
        return $this->hasMany('DM\Models\V1\AdCategory\AdCategory', 'cat_id');
    }

    /**
     * An Subcategory is owned by a ad category - subcategory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listingSubcategories()
    {
        return $this->hasMany('DM\Models\V1\AdCategory\AdCategory', 'sub_id');
    }

    /**
     * Slug eloquent accessor.
     *
     * @return string
     */
    public function getsubcategorySlugAtAttribute()
    {
        return $this->slug;
    }

    /**
     * total posts in categories by category eloquent accessor.
     *
     * @return int Total
     */
    public function getCountOfCategoriesAtAttribute()
    {
        return $this->listingCategories ?
       (int) $this->listingCategories->count() : 0;
    }

    /**
     * total posts in categories by subcategory eloquent accessor.
     *
     * @return int Total
     */
    public function getCountOfSubCategoriesAtAttribute()
    {
        return $this->listingSubcategories ?
       (int) $this->listingSubcategories->count() : 0;
    }

    /**
     * Sum all categories/subcategories eloquent accessor.
     *
     * @return int Total
     */
    public function getAllCountOfCategoriesAtAttribute()
    {
        $countCategories = $this->listingCategories ?
       (int) $this->listingCategories->count() : 0;

        $countSubCategories = $this->listingSubcategories ?
       (int) $this->listingSubcategories->count() : 0;

        return $countCategories + $countSubCategories;
    }

    /**
     * Replace subcategory name to new value.
     *
     * @return string
     */
    public function getCategoryNameReplaceAtAttribute()
    {
        return str_replace(['(A)', '(N)', '(V)', '(B)', '(T)', '(D)', '(U)', '(L)'], '', $this->name);
    }
}
