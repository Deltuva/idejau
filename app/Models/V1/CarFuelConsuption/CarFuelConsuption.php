<?php

namespace DM\Models\V1\CarFuelConsuption;

use Illuminate\Database\Eloquent\Model;

class CarFuelConsuption extends Model
{
    protected $table = 'car_fuelconsuptions';
    protected $primaryKey = 'car_consuption_id';
    protected $fillable = ['car_consuption_name'];
    public $timestamps = false;
}
