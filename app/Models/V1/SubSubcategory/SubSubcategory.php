<?php

namespace DM\Models\V1\SubSubcategory;

use Illuminate\Database\Eloquent\Model;

class SubSubcategory extends Model
{
    protected $table = 'subsubcategories';

    public $timestamps = false;

    /**
    * An SubSubcategory is owned by a post category.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function listingCategoriesSubsubcategories()
    {
        return $this->hasMany('DM\Models\V1\AdCategory\AdCategory', 'subsub_id');
    }

    /**
     * Slug eloquent accessor
     *
     * @return string
     */
    public function getsubcategorySlugAtAttribute()
    {
        return $this->slug;
    }

    /**
     * total posts in subcategories eloquent accessor
     *
     * @return int Total
     */
    public function getCountOfSubSubCategoriesAtAttribute()
    {
        return $this->listingCategoriesSubsubcategories ?
       (int) $this->listingCategoriesSubsubcategories->count() : 0;
    }

    /**
     * Replace category name to new value
     *
     * @return string
     */
    public function getCategoryNameReplaceAtAttribute()
    {
        return str_replace(['(A)', '(N)', '(V)', '(B)', '(T)', '(D)', '(U)', '(L)'], '', $this->name);
    }
}
