<?php

namespace DM\Models\V1\CarDoor;

use Illuminate\Database\Eloquent\Model;

class CarDoor extends Model
{
    protected $table = 'car_doors';
    protected $primaryKey = 'car_door_id';
    protected $fillable = ['car_door_name'];
    public $timestamps = false;
}
