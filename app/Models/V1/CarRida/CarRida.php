<?php

namespace DM\Models\V1\CarRida;

use Illuminate\Database\Eloquent\Model;

class CarRida extends Model
{
    protected $table = 'car_ridas';
    protected $primaryKey = 'car_rida_id';
    protected $fillable = ['car_rida_name'];
    public $timestamps = false;
}
