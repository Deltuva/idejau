<?php

namespace DM\Models\V1\Option;

use Illuminate\Database\Eloquent\Model;

class Option extends Model implements OptionTypeInterface
{
    protected $table = 'options';
    protected $primaryKey = 'option_id';
    protected $fillable = ['option_name'];
    public $timestamps = false;
}
