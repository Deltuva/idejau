<?php

namespace DM\Models\V1\Option;

interface OptionTypeInterface
{
    const CATEGORY_CAR = 1; //car
    const CATEGORY_ESTATE = 2; //estate
    const CATEGORY_APARTMENT = 3; //apartment
    const CATEGORY_COMPUTER = 4; //computer
    const CATEGORY_ADULT = 5; //adult
}
