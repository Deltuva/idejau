<?php

namespace DM\Models\V1\CarFuel;

use Illuminate\Database\Eloquent\Model;

class CarFuel extends Model
{
    protected $table = 'car_fuels';
    protected $primaryKey = 'car_fuel_id';
    protected $fillable = ['car_fuel_name'];
    public $timestamps = false;
}
