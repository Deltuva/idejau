<?php

namespace DM\Models\V1\CarEngine;

use Illuminate\Database\Eloquent\Model;

class CarEngine extends Model
{
    protected $table = 'car_engines';
    protected $primaryKey = 'car_engine_id';
    protected $fillable = ['car_engine_name'];
    public $timestamps = false;
}
