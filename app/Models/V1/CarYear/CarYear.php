<?php

namespace DM\Models\V1\CarYear;

use Illuminate\Database\Eloquent\Model;

class CarYear extends Model
{
    protected $table = 'car_years';
    protected $primaryKey = 'car_year_id';
    public $timestamps = false;
}
