<?php

namespace DM\Models\V1\AdStar;

use Illuminate\Database\Eloquent\Model;

class AdStar extends Model
{
    protected $table = 'ad_stars';
    protected $primaryKey = 'ad_star_id';
    public $timestamps = false;
}
