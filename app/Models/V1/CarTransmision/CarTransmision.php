<?php

namespace DM\Models\V1\CarTransmision;

use Illuminate\Database\Eloquent\Model;

class CarTransmision extends Model
{
    protected $table = 'car_transmisions';
    protected $primaryKey = 'car_transmision_id';
    protected $fillable = ['car_transmision_name'];
    public $timestamps = false;
}
