<?php

namespace DM\Models\V1\CarModel;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    protected $table = 'car_models';
    protected $primaryKey = 'car_model_id';
    protected $fillable = ['car_make_id', 'car_model_name', 'car_model_year'];
    public $timestamps = false;

    /**
     * Car Make.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleMake()
    {
        return $this->hasOne('DM\Models\V1\CarMake\CarMake', 'car_make_id', 'car_make_id');
    }
}
