<?php

namespace DM\Models\V1\CarClimate;

use Illuminate\Database\Eloquent\Model;

class CarClimate extends Model
{
    protected $table = 'car_climates';
    protected $primaryKey = 'car_climate_id';
    protected $fillable = ['car_climate_name'];
    public $timestamps = false;
}
