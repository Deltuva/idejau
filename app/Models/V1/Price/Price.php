<?php

namespace DM\Models\V1\Price;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';
    protected $primaryKey = 'price_id';
    public $timestamps = false;
}
