<?php

namespace DM\Models\V1\Category;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['name', 'slug'];
    public $timestamps = false;

    /**
     * An Category is owned by a post category category_id.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
    {
        return $this->hasMany('DM\Models\V1\Subcategory\Subcategory', 'category_id', 'id');
    }

    /**
     * Category Id eloquent accessor.
     *
     * @return int
     */
    public function getCategoryIdAtAttribute()
    {
        return $this->id;
    }

    /**
     *  total posts categories/subcategories/subsubcategories eloquent accessor.
     *
     * @return int Total
     */
    public function getCountOfCategoriesAtAttribute()
    {
        $count = DB::select('
          SELECT c.name,
          SUM((SELECT COUNT(v.ad_category_id)
              FROM categories t
              LEFT JOIN ad_categories v ON v.cat_id = t.id
              WHERE t.id = c.id
           ) + (SELECT COUNT(v.ad_category_id)
              FROM subcategories t
              LEFT JOIN ad_categories v ON v.sub_id = t.id
              WHERE t.category_id = c.id)
             + (SELECT COUNT(v.ad_category_id)
                 FROM subsubcategories t
                 LEFT JOIN ad_categories v ON v.subsub_id = t.id
                 WHERE t.subcategory_id = c.id)) AS total
        FROM categories c
        WHERE c.id = "'.$this->categoryIdAt.'"
        GROUP BY c.id
        ');

        return $count[0] ?
        (int) $count[0]->total : 0;
    }

    /**
     * Slug eloquent accessor.
     *
     * @return string
     */
    public function getcategorySlugAtAttribute()
    {
        return $this->slug;
    }

    /**
     * Replace category name to new value.
     *
     * @return string
     */
    public function getCategoryNameReplaceAtAttribute()
    {
        return str_replace(['(A)', '(N)', '(V)', '(B)', '(T)', '(D)', '(U)', '(L)'], '', $this->name);
    }
}
