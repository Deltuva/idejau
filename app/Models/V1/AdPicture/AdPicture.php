<?php

namespace DM\Models\V1\AdPicture;

use Illuminate\Database\Eloquent\Model;

class AdPicture extends Model
{
    protected $table = 'ad_pics';
    protected $primaryKey = 'adpic_id';
    public $timestamps = false;

    /**
     * Scope Latest picture orderBy query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderedByLatestPicture($query)
    {
        return $query->latest();
    }
}
