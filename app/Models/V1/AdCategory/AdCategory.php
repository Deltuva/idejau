<?php

namespace DM\Models\V1\AdCategory;

use Illuminate\Database\Eloquent\Model;

class AdCategory extends Model
{
    protected $table = 'ad_categories';

    /**
     * An Post is owned by a category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function ads()
    {
        return $this->hasMany('DM\Ad', 'ad_id', 'ad_id');
    }

    /**
     * Ad Posts Picture.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function pictureRelation()
    {
        return $this->hasOne('DM\Models\V1\AdPicture\AdPicture')
            ->orderedByLatestPicture();
    }

    /**
     * Ad Posts Pictures.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function picturesRelation()
    {
        return $this->hasMany('DM\Models\V1\AdPicture\AdPicture');
    }

    /**
     * Ad Listing Pictures.
     *
     * @return total
     */
    public function picturesCountRelation()
    {
        return $this->pictureRelation()
            ->selectRaw('adpic_picid, COUNT(*) as totalPics')
            ->groupBy('adpic_picid');
    }

    /**
     * Pictures eloquent accessor.
     *
     * @return string
     */
    public function getAdPictureAtAttribute()
    {
        return $this->pictureRelation ? $this->pictureRelation->adpic_file : '';
    }

    /**
     * Pictures Count eloquent accessor.
     *
     * @return mixed
     */
    public function getAdPictureCountAtAttribute()
    {
        return $this->picturesCountRelation ? $this->picturesCountRelation->totalPics : 0;
    }

    /**
     * Pictures Block hide eloquent accessor.
     *
     * @return string
     */
    public function getAdPictureHideAtAttribute()
    {
        return $this->pictureRelation ? $this->pictureRelation->adpic_show : false;
    }

    /**
     * Users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function users()
    {
        return $this->hasMany('DM\User', 'id');
    }

    /**
     * City.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function cities()
    {
        return $this->hasMany('DM\City', 'id');
    }

    /**
     * Scope Total stars orderBy query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderedByRatingStarTotal($query)
    {
        return $query->orderBy('starTotal', 'desc');
    }

    /**
     * Scope Recent ads orderBy query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderedByRecent($query)
    {
        return $query->orderBy('posts.created_at', 'desc');
    }

    /**
     * Stars.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function starRatingSum()
    {
        return $this->hasMany('DM\PostStar', 'post_id')
            ->selectRaw('post_id, SUM(star_amount) as starTotal')
            ->groupBy('post_id');
    }

    /**
     * Scope Buyed stars orderBy query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderByBuyedStars($query)
    {
        return $query->orderBy('totalBuyedstars', 'desc');
    }

    /**
     * Scope a query owned by active.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivePublish($query)
    {
        return $query->where('ads.ad_active', 1);
    }
}
