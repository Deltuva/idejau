<?php

namespace DM\Models\V1\CarWheel;

use Illuminate\Database\Eloquent\Model;

class CarWheel extends Model
{
    protected $table = 'car_wheels';
    protected $primaryKey = 'car_wheel_id';
    protected $fillable = ['car_wheel_name'];
    public $timestamps = false;
}
