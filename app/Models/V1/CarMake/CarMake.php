<?php

namespace DM\Models\V1\CarMake;

use Illuminate\Database\Eloquent\Model;

class CarMake extends Model
{
    protected $table = 'car_makes';
    protected $primaryKey = 'car_make_id';
    protected $fillable = ['car_make_name'];
    public $timestamps = false;
}
