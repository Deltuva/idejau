<?php

namespace DM\Models\V1\Ad;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use DM\Traits\FavoriteTrait;
use DM\Traits\AdDataTrait;
use Carbon\Carbon;

class Ad extends Model
{
    use Filterable;
    use FavoriteTrait;
    use AdDataTrait;

    const LATEST_NAME_LIMIT = 30;
    const DESCRIPTION_LIMIT = 100;
    const DIFF_IN_DAYS = 1;
    const DIFF_IN_MINUTES = 5;
    const ITEMS_PER_PAGE = 10;
    const TYPE_USER_PRIVATE = 'private';
    const TYPE_USER_COMPANY = 'company';

    protected $table = 'ad';
    protected $primaryKey = 'ad_id';

    public static $columns = [
        'ad_id', 'type_id', 'ad_price', 'created_at'
    ];

    /**
     * Get One favorite
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function favorite()
    {
        return $this->hasOne('DM\Models\V1\UserFavorite\UserFavorite', 'ad_id', 'ad_id');
    }
    /**
     * Ad is owned by a category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('DM\Models\V1\Category\Category', 'category_id');
    }

    /**
     * An Ad is owned by a category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function adListingCategory()
    {
        return $this->hasMany('DM\Models\V1\AdCategory\AdCategory', 'ad_id', 'ad_id');
    }

    /**
     * Scope orderBy CreatedAt query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderedByCreated($query)
    {
        return $query->orderBy('ad.created_at', 'desc');
    }

    /**
     * Get User owned by id.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function userSingleId()
    {
        return $this->hasOne('DM\Models\V1\User\User', 'id', 'user_id');
    }

    /**
     * Get User Company owned by id.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function userCompanySingleId()
    {
        return $this->hasOne('DM\Models\V1\UserCompany\UserCompany', 'user_id', 'user_id');
    }

    /**
     * Car Make.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleMake()
    {
        return $this->hasOne('DM\Models\V1\CarMake\CarMake', 'car_make_id', 'car_make_id');
    }

    /**
     * Car Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleModel()
    {
        return $this->hasOne('DM\Models\V1\CarModel\CarModel', 'car_model_id', 'car_model_id');
    }

    /**
     * Car Body.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleBody()
    {
        return $this->hasOne('DM\Models\V1\CarBody\CarBody', 'car_body_id', 'car_body_id');
    }

    /**
     * Car Climate.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleClimate()
    {
        return $this->hasOne('DM\Models\V1\CarClimate\CarClimate', 'car_climate_id', 'car_climate_id');
    }

    /**
     * Car Color.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleColor()
    {
        return $this->hasOne('DM\Models\V1\CarColor\CarColor', 'car_color_id', 'car_color_id');
    }

    /**
     * Car Damaged.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleDamaged()
    {
        return $this->hasOne('DM\Models\V1\CarDamaged\CarDamaged', 'car_damage_id', 'car_damage_id');
    }

    /**
     * Car Door.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleDoor()
    {
        return $this->hasOne('DM\Models\V1\CarDoor\CarDoor', 'car_door_id', 'car_door_id');
    }

    /**
     * Car Engine.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleEngine()
    {
        return $this->hasOne('DM\Models\V1\CarEngine\CarEngine', 'car_engine_id', 'car_engine_id');
    }

    /**
     * Car First Country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleFirstCountry()
    {
        return $this->hasOne('DM\Models\V1\CarFirstCountry\CarFirstCountry', 'car_country_id', 'car_country_id');
    }

    /**
     * Car Fuel.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleFuel()
    {
        return $this->hasOne('DM\Models\V1\CarFuel\CarFuel', 'car_fuel_id', 'car_fuel_id');
    }

    /**
     * Car Fuel Consuption.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleFuelConsuption()
    {
        return $this->hasOne('DM\Models\V1\CarFuelConsuption\CarFuelConsuption', 'car_consuption_id', 'car_consuption_id');
    }

    /**
     * Car Gearbox.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleGearbox()
    {
        return $this->hasOne('DM\Models\V1\CarGearbox\CarGearbox', 'car_gearbox_id', 'car_gearbox_id');
    }

    /**
     * Car Year.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleYear()
    {
        return $this->hasOne('DM\Models\V1\CarYear\CarYear', 'car_year_id', 'car_year_id');
    }

    /**
     * Car Month.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleMonth()
    {
        return $this->hasOne('DM\Models\V1\CarMonth\CarMonth', 'car_month_id', 'car_month_id');
    }

    /**
     * Car Power.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSinglePower()
    {
        return $this->hasOne('DM\Models\V1\CarPower\CarPower', 'car_power_id', 'car_power_id');
    }

    /**
     * Car Rida.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleRida()
    {
        return $this->hasOne('DM\Models\V1\CarRida\CarRida', 'car_rida_id', 'car_rida_id');
    }

    /**
     * Car Transmision.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleTransmision()
    {
        return $this->hasOne('DM\Models\V1\CarTransmision\CarTransmision', 'car_transmision_id', 'car_transmision_id');
    }

    /**
     * Car Wheel.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function carSingleWheel()
    {
        return $this->hasOne('DM\Models\V1\CarWheel\CarWheel', 'car_wheel_id', 'car_wheel_id');
    }

    /**
     * Define The Default Model Filter.
     *
     * @method modelFilter
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInputSearchFilter($query)
    {
        return $query->filter(request()->all(), \DM\Models\ModelFilters\AdFilter::class);
    }

    /**
     * Scope UserType Person Count query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUsersTypePersonCount($query)
    {
        return $query->leftJoin('users', 'users.id', '=', 'posts.post_uid')
            ->where('users.user_type', 'person')->count();
    }

    /**
     * Scope UserType Company Count query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUsersTypeCompanyCount($query)
    {
        return $query->leftJoin('users', 'users.id', '=', 'posts.post_uid')
            ->where('users.user_type', 'company')->count();
    }

    /**
     * Ad Posts Picture.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function pictureRelation()
    {
        return $this->hasOne('DM\Models\V1\AdPicture\AdPicture', 'ad_id', 'ad_id')
            ->orderedByLatestPicture();
        ;
    }

    /**
     * Ad Posts Pictures.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function picturesRelation()
    {
        return $this->hasMany('DM\Models\V1\AdPicture\AdPicture', 'ad_id', 'ad_id');
    }

    /**
     * Ad Listing Pictures.
     *
     * @return total
     */
    public function picturesCountRelation()
    {
        return $this->pictureRelation()
            ->selectRaw('ad_id, COUNT(*) as totalPics')
            ->groupBy('ad_id');
    }

    /**
     * Pictures eloquent accessor.
     *
     * @return string
     */
    public function getAdPictureAtAttribute()
    {
        return $this->pictureRelation ? $this->pictureRelation->adpic_file : '';
    }

    /**
     * Pictures Count eloquent accessor.
     *
     * @return mixed
     */
    public function getAdPictureCountAtAttribute()
    {
        return $this->picturesCountRelation ? $this->picturesCountRelation->totalPics : 0;
    }

    /**
     * Pictures Block hide eloquent accessor.
     *
     * @return string
     */
    public function getAdPictureHideAtAttribute()
    {
        return $this->pictureRelation ? $this->pictureRelation->adpic_show : false;
    }

    /**
     * Display image file.
     *
     * @return image
     */
    public function getImage()
    {
        if ($this->adPictureAt) {
            if ($this->adPictureHideAt == false) {
                return asset('images/hide-image.jpg');
            } else {
                return url('images/photos/'.$this->adPictureAt);
            }
        } else {
            return asset('images/no-image.jpg');
        }
    }

    /**
     * Display company image file.
     *
     * @return image
     */
    public function getCompanyImage()
    {
        if (!is_null($this->userCompanySingleId->logo)) {
            //return asset('images/companies/muuray.png');
            return url('images/companies/'.$this->userCompanySingleId->logo);
        }
    }

    /**
     * Display company name.
     *
     * @return string|IV,MB,UAB,AB
     */
    public function getCompanyName()
    {
        if (!is_null($this->userCompanySingleId->name) || !is_null($this->userCompanySingleId->type)) {
            return $this->userCompanySingleId->type.' '.$this->userCompanySingleId->name;
        }
    }

    /**
     * Display user company detail
     *
     * @return string|html
     */
    public function getCompanyDetail($userId = null)
    {
        $userId = $this->isCurrentLoggedUser();

        if ($this->userSingleId->user_type == self::TYPE_USER_COMPANY) {
            $detailWidget = '<span class="company">';
            $detailWidget .= '<a target="_blank" href="#">';
            $detailWidget .= '<img src="'.$this->getCompanyImage().'" alt="'.$this->getCompanyName().'">';
            $detailWidget .= '</a>';
            $detailWidget .= '</span>';

        } else {
            $detailWidget = '<span class="created">';
            $detailWidget .= $this->getTimeAgoEditedStatus();
            $detailWidget .= $this->getTimeAgoDays();
            $detailWidget .= '</span>';
        }

        return $detailWidget ? $detailWidget : '';
    }

    /**
     * Current User Id
     *
     * @return integer
     */
    public function isCurrentLoggedUser($userId = null)
    {
        // user id when logged in
        if (auth()->check()) {
            $userId = auth()->user()->id;
        }
        return $userId;
    }

    /**
     * Total Pics.
     *
     * @return mixed
     */
    public function getImageCount()
    {
        if ($this->adPictureCountAt > 1) {
            return '<i class="fa fa-camera"></i> '.$this->adPictureCountAt;
        }
    }

    /**
     * Display Title Name.
     *
     * @return string
     */
    public function getTitleName()
    {
        if (!is_null($this->carSingleMake->car_make_name) || !is_null($this->carSingleBody->car_body_name) || !is_null($this->carSingleEngine->car_engine_name)) {
            return $this->carSingleMake->car_make_name.' '.@$this->carSingleModel->car_model_name.', '.$this->carSingleEngine->car_engine_name.' l, '.$this->carSingleBody->car_body_name;
        }
    }

    /**
     * Display formated price.
     *
     * @return integer|decimal
     */
    public function getFormatedPrice()
    {
        if (!is_null($this->ad_price) || $this->ad_price > 0) {
            return $this->ad_price;
        }
    }

    /**
     * Description cut "...".
     *
     * @return string
     */
    public function getLimitDescription()
    {
        if (!is_null($this->ad_description)) {
            return str_limit($this->ad_description, self::DESCRIPTION_LIMIT);
        }
    }

    /**
     * Display More description
     *
     * @return string
     */
    public function getMoreDescription()
    {
        if ($this->detail_id > 0) {
            //cars category
            if ($this->detail_id === 1) {
                if (!is_null($this->carSingleYear->car_year_name)) {
                    $car_year = $this->carSingleYear->car_year_name.'-';
                }
                if (!is_null($this->carSingleMonth->car_month_name)) {
                    $car_month = $this->carSingleMonth->car_month_name.'m.<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleFuel->car_fuel_name)) {
                    $car_fuel = $this->carSingleFuel->car_fuel_name.'<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleEngine->car_engine_name)) {
                    $car_engine = $this->carSingleEngine->car_engine_name.' l<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleGearbox->car_gearbox_name)) {
                    $car_gearbox = $this->carSingleGearbox->car_gearbox_name.'<span class="dec_space">|</span>';
                }
                if (!is_null($this->car_km)) {
                    $car_rida = $this->car_km.' km<span class="dec_space">|</span>';
                }
                if (!is_null($this->carSingleDoor->car_door_name)) {
                    $car_door = $this->carSingleDoor->car_door_name;
                }
                return $car_year.$car_month.$car_fuel.$car_engine.$car_gearbox.$car_rida.$car_door;
            }
        }
    }

    /**
     * Display lastname.
     *
     * @return string
     */
    public function getLatestName()
    {
        if (!is_null($this->ad_name)) {
            return str_limit($this->ad_name, self::LATEST_NAME_LIMIT);
        }
    }

    /**
     * Display City Name.
     *
     * @return string
     */
    public function getLocationName()
    {
        if (!is_null($this->cityName)) {
            return $this->cityName;
        }
    }

    /**
     * Display User Type.
     *
     * @return string
     */
    public function getUserTypeName()
    {
        if (!is_null($this->user_type)) {
            if ($this->user_type == self::TYPE_USER_PRIVATE) {
                // return trans('home.ad.user_type_private');
            } elseif ($this->user_type == self::TYPE_USER_COMPANY) {
                return trans('home.ad.user_type_company');
            }
        }
    }

    /**
     * Display latest view status.
     *
     * @return true
     */
    public function getLatestVisitStatus()
    {
        $lastViewAd = [
          'post_id' => $this->id,
        ];

        if (session()->has('last_view')) {
            $lastViewArray = session('last_view');

            foreach ($lastViewArray as $k => $v) {
                if ($v['post_id'] == $lastViewAd['post_id']) {
                    $this->visited = true;
                    break;
                }
            }
        }

        return $this->visited;
    }

    /**
     * Display html stars counter widget.
     * 
     * @return string|html
     */
    public function getStarsCountWidget()
    {
        $iconWidget = '<div class="star-wrap">';
        $iconWidget .= '<div class="star-i">';
        $iconWidget .= '<i class="fa fa-star" aria-hidden="true"></i>'.$this->starTotal;
        $iconWidget .= '</div>';
        $iconWidget .= '</div>';

        return $this->starTotal ? $iconWidget : '';
    }

    /**
     * Display carbon time today.
     *
     * @return mixed|html
     */
    public function getTodayWidget()
    {
        $createdToday = new Carbon($this->created_at, 'Europe/Vilnius');
        $now = Carbon::now();

        $iconWidget = '<div class="new">';
        $iconWidget .= '<div class="icon">';
        $iconWidget .= '</div>';
        $iconWidget .= '</div>';

        $todayStatus = ($createdToday->diff($now)->days < self::DIFF_IN_DAYS) ? $iconWidget : null;

        return $todayStatus;
    }

    /**
     * Display carbon time ago in days.
     *
     * @return mixed|html
     */
    public function getTimeAgoDays()
    {
        setlocale(LC_TIME, 'Lithuania');
        Carbon::setLocale('lt');

        $createdDate = new Carbon($this->created_at, 'Europe/Vilnius');
        //$now = Carbon::now();
        $date = Carbon::createFromTimeStamp(strtotime($createdDate))->diffForHumans();

        if ($createdDate->diffInDays() < self::DIFF_IN_DAYS) {
            $agoTime = $createdDate->diffForHumans();
            $replacedAgoTime = ucfirst(str_replace(['valandą', 'valandas', 'valandų'], 'val.', $agoTime));
            $iconWidget = '<span class="time today">';
            $iconWidget .= '<i class="fa fa-clock-o" aria-hidden="true"></i> '.$replacedAgoTime;
            $iconWidget .= '</span>';
        } else {
            $agoTime = 'Prieš <b>'.$createdDate->diffInDays().'</b> d.';
            $iconWidget = '<span class="time">';
            $iconWidget .= $agoTime;
            $iconWidget .= '</span>';
        }

        $agoStatus = $iconWidget;

        return $agoStatus;
    }

    /**
     * Display carbon time ago in minutes status message.
     *
     * @return mixed|html
     */
    public function getTimeAgoEditedStatus()
    {
        $updatedDate = new Carbon($this->updated_at, 'Europe/Vilnius');

        //$now = Carbon::now('Europe/Vilnius');
        if ($updatedDate->diffInMinutes() <= self::DIFF_IN_MINUTES) { //5 minutes
            $iconWidget = '<span class="time edited">';
            $iconWidget .= '<i class="fa fa-pencil-square" aria-hidden="true"></i> ';
            $iconWidget .= trans('home.ad.updated_status');
            $iconWidget .= '</span>';
        } else {
            $iconWidget = '';
        }

        $agoStatus = $iconWidget;

        return $agoStatus;
    }
}
