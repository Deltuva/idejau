<?php

namespace DM\Models\V1\UserFavorite;

use Illuminate\Database\Eloquent\Model;

class UserFavorite extends Model
{
    protected $table = 'users_favorites';
    protected $primaryKey = 'favorite_id';
    protected $fillable = ['user_id', 'ad_id'];
    public $timestamps = false;

    /**
     * Get One favorite ad
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function ad()
    {
        return $this->hasOne('DM\Models\V1\Ad\Ad', 'ad_id', 'ad_id');
    }
}