<?php

namespace DM\Models\V1\CarFirstCountry;

use Illuminate\Database\Eloquent\Model;

class CarFirstCountry extends Model
{
    protected $table = 'car_firstcountries';
    protected $primaryKey = 'car_country_id';
    protected $fillable = ['car_country_name'];
    public $timestamps = false;
}
