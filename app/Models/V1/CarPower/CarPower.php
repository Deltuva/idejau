<?php

namespace DM\Models\V1\CarPower;

use Illuminate\Database\Eloquent\Model;

class CarPower extends Model
{
    protected $table = 'car_powers';
    protected $primaryKey = 'car_power_id';
    protected $fillable = ['car_power_name'];
    public $timestamps = false;
}
