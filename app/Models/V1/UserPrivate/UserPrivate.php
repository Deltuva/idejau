<?php

namespace DM\Models\V1\UserPrivate;

use Illuminate\Database\Eloquent\Model;

class UserPrivate extends Model
{
    protected $table = 'users_privates';
    protected $primaryKey = 'private_id';
    public $timestamps = false;
}
