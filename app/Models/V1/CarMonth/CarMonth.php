<?php

namespace DM\Models\V1\CarMonth;

use Illuminate\Database\Eloquent\Model;

class CarMonth extends Model
{
    protected $table = 'car_months';
    protected $primaryKey = 'car_month_id';
    protected $fillable = ['car_month_name'];
    public $timestamps = false;
}
