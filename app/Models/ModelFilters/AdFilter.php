<?php namespace DM\Models\ModelFilters;

use EloquentFilter\ModelFilter;
use DM\Sanitize\CleanInput;

class AdFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relatedModel => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    /**
     * Price Min filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function priceMin($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('ad_price', '>=', $_clean);
    }

    /**
     * Price Max filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function priceMax($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('ad_price', '<=', $_clean);
    }

    /**
     * Car make filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function make($id)
    {
        //$_clean = CleanInput::cleaner($id, 'num');
        return $this->whereIn('car_make_id', $id);
    }

    /**
     * Car model filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function model($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_model_id', $_clean);
    }

    /**
     * Car body filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function body($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_body_id', $_clean);
    }

    /**
     * Car climate filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function climate($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_climate_id', $_clean);
    }

    /**
     * Car color filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function color($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_color_id', $_clean);
    }

    /**
     * Car damage filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function damage($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_damage_id', $_clean);
    }

    /**
     * Car door filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function door($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_door_id', $_clean);
    }

    /**
     * Car engine from filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function enginef($id)
    {
        return $this->whereHas('carSingleEngine', function ($query) use ($id) {
            return $query->where('car_engine_value', '>=', $id);
        });
    }

    /**
     * Car engine to filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function enginet($id)
    {
        return $this->whereHas('carSingleEngine', function ($query) use ($id) {
            return $query->where('car_engine_value', '<=', $id);
        });
    }

    /**
     * Car first country filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function country($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_country_id', $_clean);
    }

    /**
     * Car fuel filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function fuel($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_fuel_id', $_clean);
    }

    /**
     * Car gearbox filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function gearbox($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_gearbox_id', $_clean);
    }

    /**
     * Car power from filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function powerf($id)
    {
        return $this->whereHas('carSinglePower', function ($query) use ($id) {
            return $query->where('car_power_value', '>=', $id);
        });
    }

    /**
     * Car power to filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function powert($id)
    {
        return $this->whereHas('carSinglePower', function ($query) use ($id) {
            return $query->where('car_power_value', '<=', $id);
        });
    }

    /**
     * Car rida from filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function kmf($id)
    {
        return $this->whereHas('carSingleRida', function ($query) use ($id) {
            return $query->where('car_rida_value', '>=', $id);
        });
    }

    /**
     * Car rida to filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function kmt($id)
    {
        return $this->whereHas('carSingleRida', function ($query) use ($id) {
            return $query->where('car_rida_value', '<=', $id);
        });
    }

    /**
     * Car transmision filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function transmision($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_transmision_id', $_clean);
    }

    /**
     * Car wheel filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function wheel($id)
    {
        $_clean = CleanInput::cleaner($id, 'num');
        return $this->where('car_wheel_id', $_clean);
    }
}
