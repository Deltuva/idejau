<?php

namespace DM\Repositories\V1\Type;

interface UserTypeInterface
{
    const USER_TYPE_PRIVATE = 'private';
    const USER_TYPE_COMPANY = 'company';
}
