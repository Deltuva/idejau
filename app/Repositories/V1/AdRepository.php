<?php
namespace DM\Repositories\V1;

use DM\Abstracts\Repository as AbstractRepository;
use DM\Repositories\V1\InterfaceMethods\AdRepositoryInterface;
use DM\Repositories\V1\Type\UserTypeInterface;
use DM\Models\V1\Ad\Ad as Ad;

class AdRepository extends AbstractRepository implements AdRepositoryInterface, UserTypeInterface
{
    protected $model;

    protected $relations = [
      'adListingCategory',
      'carSingleMake',
      'carSingleModel',
      'carSingleBody',
      'carSingleClimate',
      'carSingleColor',
      'carSingleDamaged',
      'carSingleDoor',
      'carSingleEngine',
      'carSingleFirstCountry',
      'carSingleFuel',
      'carSingleFuelConsuption',
      'carSingleGearbox',
      'carSingleYear',
      'carSingleMonth',
      'carSinglePower',
      'carSingleTransmision',
      'carSingleWheel'
    ];

    /**
     * ListingRepository constructor
     *
     * @param AdListing $adListing
     */
    public function __construct(Ad $ads)
    {
        $this->model = $ads;
    }

    /**
     * Get Model Columns for data
     *
     * @return \DM\Models\V1\Listing\Ad
     */
    public function getDataColumns()
    {
        return $this->model::$columns;
    }

    /**
     * Get All list
     *
     * @return \DM\Models\V1\Ad\Ad
     */
    public function getAllPublicList($conditions = [])
    {
        $columns = ['ad.*',
          'users.user_type',
          'users_companies.logo as companyImage',
          'users_companies.name as companyName',
          'users_companies.type as companyType',
          'cities.city_name as cityName'];

        $qr = $this->model->select($columns)
          ->leftJoin('users', 'users.id', '=', 'ad.user_id')
          ->leftJoin('users_companies', 'users_companies.user_id', '=', 'ad.user_id')
          ->leftJoin('cities', 'cities.id', '=', 'ad.location_id')
          ->where('ad.ad_active', false)->with($this->relations);

        if ($conditions) {
            if (isset($conditions['cat'])) {
                $catid = $conditions['cat'];
                $qr->whereHas('adListingCategory', function ($q) use ($catid) {
                    if ($catid) {
                        $q->where('cat_id', $catid);
                    }
                });
            } elseif (isset($conditions['sub'])) {
                $subid = $conditions['sub'];
                $qr->whereHas('adListingCategory', function ($q) use ($subid) {
                    if ($subid) {
                        $q->where('sub_id', $subid);
                    }
                });
            } elseif (isset($conditions['subsub'])) {
                $subsubid = $conditions['subsub'];
                $qr->whereHas('adListingCategory', function ($q) use ($subsubid) {
                    if ($subsubid) {
                        $q->where('subsub_id', $subsubid);
                    }
                });
            }
        }
        $query = $qr->searchByRequestParams()
        ->inputSearchFilter();

        return self::pageData($query);
    }

    /**
     * Get New Array Collection
     *
     * @return Array
     */
    public function contentTransform($take = 6)
    {
        $adlist = $this->model
          ->take($take)
          ->orderedByCreated();

        return $adlist;
    }

    /**
     * Get All Ad list
     *
     * @return \DM\Models\V1\Listing\Ad
     */
    public function getLatestAdList($take = 6)
    {
        $adlist = $this->model
          ->take($take)
          ->orderedByCreated();

        return $adlist;
    }

    /**
     * Get total ad count.
     *
     * @param array $conditions
     *
     * @return int Total
     */
     public function getAdTotal($conditions = [])
     {
         $qr = $this->model->leftJoin('users', 'users.id', '=', 'ad.user_id')
               ->with($this->relations)
                   ->searchByRequestParams();
         if ($conditions) {
             foreach ($conditions as $key => $value) {
                 if (is_array($value)) {
                     $qr->where($key, $value[0], $value[1]);
                 } else {
                     $qr->where($key, $value);
                 }
             }
         }

         return $qr->count();
     }

     /**
      * Get total by private user
      *
      * @return int Total
      */
      public function getCountPrivateUsers()
      {
          $qr = $this->model->leftJoin('users', 'users.id', '=', 'ad.user_id')
                ->where('users.user_type', $this->model::TYPE_USER_PRIVATE);

          return $qr->count();
      }

      /**
       * Get total by company user
       *
       * @return int Total
       */
       public function getCountCompanyUsers()
       {
           $qr = $this->model->leftJoin('users', 'users.id', '=', 'ad.user_id')
                 ->where('users.user_type', $this->model::TYPE_USER_COMPANY);

           return $qr->count();
       }

    /**
     * @param $query
     * @param $perPage
     *
     * @return mixed
     */
    public function pageData($query, $perPage = 10)
    {
        if (!is_null($perPage)) {
            $qr = $query
              ->paginate($perPage);
        } else {
            $qr = $query->get();
        }

        return $qr;
    }
}
