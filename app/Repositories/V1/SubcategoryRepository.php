<?php
namespace DM\Repositories\V1;

use DM\Abstracts\Repository as AbstractRepository;
use DM\Repositories\V1\InterfaceMethods\SubcategoryRepositoryInterface;
use DM\Models\V1\Subcategory\Subcategory;
use DM\Models\V1\Ad\Ad;

class SubcategoryRepository extends AbstractRepository implements SubcategoryRepositoryInterface
{
    protected $model;

    /**
     * SubcategoryRepository constructor
     *
     * @param Subcategory $subcategory
     */
    public function __construct(Subcategory $subcategory)
    {
        $this->model = $subcategory;
    }

    /**
     * Get All subcategories list
     *
     * @return \DM\Models\V1\Subcategory\Subcategory
     */
    public function getSubcategories($category)
    {
        $catInfo = $this->model
          ->whereCategoryId($category)
              ->with('listingCategories')->get();
        if (!$catInfo) {
            abort(404);
        }
        return $catInfo;
    }

    /**
     * Get Subcategory Id By Slug
     * @param $slug
     *
     * @return \DM\Models\V1\SubSubcategory\SubSubcategory
     */
    public function findIdBySlug($slug)
    {
        $slugId = $this->model->whereSlug($slug)->first();
        if (!$slugId) {
            abort(404);
        }
        return $slugId;
    }

    /**
     * Get count Ads owned by subcategory
     *
     * @param $category
     *
     * @return int Total
     */
    public function getAdTitle($subcategory)
    {
        $subcategoriesCollection = $this->model
           ->whereId($subcategory)->first();
        $subid = $subcategoriesCollection->id;
        $array = ['subid' => $subid,
             'category_title' => $subcategoriesCollection->name,
             'category_ad_count' => Ad::whereHas('adListingCategory', function ($q) use ($subid) {
                 if ($subid) {
                     $q->where('sub_id', $subid);
                 }
             })->count(),
             'category_ad_private_count' => Ad::leftJoin('users', 'users.id', '=', 'ad.user_id')
                ->whereHas('adListingCategory', function ($q) use ($subid) {
                    if ($subid) {
                        $q->where('sub_id', $subid);
                    }
                })->where('users.user_type', 'private')->count(),
             'category_ad_company_count' => Ad::leftJoin('users', 'users.id', '=', 'ad.user_id')
                ->whereHas('adListingCategory', function ($q) use ($subid) {
                    if ($subid) {
                        $q->where('sub_id', $subid);
                    }
                })->where('users.user_type', 'company')->count()
          ];

        return $array;
    }
}
