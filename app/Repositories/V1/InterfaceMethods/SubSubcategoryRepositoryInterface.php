<?php
namespace DM\Repositories\V1\InterfaceMethods;

/**
 * The ListingRepositoryInterface contains ONLY method signatures for methods
 * related to the User object.
 *
 * Note that we extend from RepositoryInterface, so any class that implements
 * this interface must also provide all the standard eloquent methods (find, all, etc.)
 */

interface SubSubcategoryRepositoryInterface extends RepositoryInterface
{
    public function getSubsubcategories($subcategory);
}
