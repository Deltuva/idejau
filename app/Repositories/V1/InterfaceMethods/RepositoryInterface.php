<?php
namespace DM\Repositories\V1\InterfaceMethods;

use DM\Repositories\V1\InterfaceMethods\RepositoryInterface;

/**
 * RepositoryInterface provides the standard functions to be expected of ANY
 * repository.
 */
interface RepositoryInterface
{
    /**
     * [create description]
     * @param  array  $attributes [description]
     * @return [type]             [description]
     */
    public function create(array $attributes);

    /**
     * [all description]
     * @param  array  $columns [description]
     * @return [type]          [description]
     */
    public function all($columns = array('*'));

    /**
     * [find description]
     * @param  [type] $id      [description]
     * @param  array  $columns [description]
     * @return [type]          [description]
     */
    public function find($id, $columns = array('*'));

    /**
     * [destroy description]
     * @param  [type] $ids [description]
     * @return [type]      [description]
     */
    public function destroy($ids);
}
