<?php
namespace DM\Repositories\V1;

use DM\Abstracts\Repository as AbstractRepository;
use DM\Models\V1\UserFavorite\UserFavorite;

class FavoriteRepository extends AbstractRepository
{
    /**
     * @var $model
     */
    protected $model;

    /**
     * FavoriteRepository constructor
     *
     * @param UserFavorite $favorites
     */
    public function __construct(UserFavorite $favorites)
    {
        $this->model = $favorites;
    }

    /**
     * Get all user favorites
     * 
     * @param $page 
     * @return array
     */
    public function getAllFavorites($page = 10) 
    {
        if (!$this->isCurrentAuthUser()) {
            return $favorites = null;
        }
        $favorites = $this->isCurrentAuthUser()
          ->favorites()
          ->orderBy('favorite_id', 'desc');

        return $favorites->paginate($page);
    }

    /**
     * Get all user favorites
     *
     * @return array
     */
    public function getCountAllFavorites() 
    {
        if (!$this->isCurrentAuthUser()) {
            return $favorites = null;
        }
        $favorites = $this->isCurrentAuthUser()
              ->favorites();

        return $favorites->count();
    }

    /**
     * Create favorite
     *
     * @param $favId
     * @return boolean
     */
    public function getCreateFavorited($favId) 
    {
        return $this->isCurrentAuthUser()->favorites()
           ->syncWithoutDetaching([$favId]);
    }

    /**
     * Create unfavorite
     *
     * @param $favId
     * @return boolean
     */
    public function getCreateUnFavorited($favId) 
    {
        return $this->isCurrentAuthUser()->favorites()
           ->detach([$favId]);
    }

    /**
     * Current Auth User
     *
     * @return object
     */
    public function isCurrentAuthUser($user = null)
    {
        // user object when logged in
        if (auth()->check()) {
            $user = auth()->user();
        }
        return $user;
    }
}
