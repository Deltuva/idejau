<?php
namespace DM\Repositories\V1;

use DM\Abstracts\Repository as AbstractRepository;
use DM\Repositories\V1\InterfaceMethods\CategoryRepositoryInterface;
use DM\Models\V1\Category\Category;
use DM\Models\V1\Ad\Ad;

class CategoryRepository extends AbstractRepository implements CategoryRepositoryInterface
{
    protected $model;

    /**
     * CategoryRepository constructor
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Get All categories list
     *
     * @return \DM\Models\V1\Category\Category
     */
    public function getCategories()
    {
        return $this->model->all();
    }

    /**
     * Get Category Id By Slug
     * @param $slug
     *
     * @return \DM\Models\V1\Category\Category
     */
    public function findIdBySlug($slug)
    {
        $slugId = $this->model->whereSlug($slug)->first();
        if (!$slugId) {
            abort(404);
        }
        return $slugId;
    }


    /**
     * Get count Ads owned by category
     *
     * @param $category
     *
     * @return int Total
     */
    public function getAdTitle($category)
    {
        $categoriesCollection = $this->model->whereId($category)->first();
        $catid = $categoriesCollection->id;
        $array = ['catid' => $catid,
             'category_title' => $categoriesCollection->name,
             'category_ad_count' => Ad::whereHas('adListingCategory', function ($q) use ($catid) {
                 if ($catid) {
                     $q->where('cat_id', $catid);
                 }
             })->count(),
             'category_ad_private_count' => Ad::leftJoin('users', 'users.id', '=', 'ad.user_id')
                ->whereHas('adListingCategory', function ($q) use ($catid) {
                    if ($catid) {
                        $q->where('cat_id', $catid);
                    }
                })->where('users.user_type', 'private')->count(),
             'category_ad_company_count' => Ad::leftJoin('users', 'users.id', '=', 'ad.user_id')
                ->whereHas('adListingCategory', function ($q) use ($catid) {
                    if ($catid) {
                        $q->where('cat_id', $catid);
                    }
                })->where('users.user_type', 'company')->count()
          ];

        return $array;
    }


    /**
     * Get sum all categories / subcategories / subsubcategories .
     *
     * @return int Total
     */
    public function getAdTotal()
    {
        return $this->model
            ->selectRaw("(SELECT c.name,
            SUM((SELECT COUNT(v.ad_category_id)
                FROM categories t
                LEFT JOIN ad_categories v ON v.cat_id = t.id
                WHERE t.id = c.id
             ) + (SELECT COUNT(v.ad_category_id)
                FROM subcategories t
                LEFT JOIN ad_categories v ON v.sub_id = t.id
                WHERE t.category_id = c.id)
               + (SELECT COUNT(v.ad_category_id)
                   FROM subsubcategories t
                   LEFT JOIN ad_categories v ON v.subsub_id = t.id
                   WHERE t.subcategory_id = c.id)) AS total)")
          ->where('id', 1)->count();
    }
}
