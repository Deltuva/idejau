<?php
namespace DM\Repositories\V1;

use DM\Abstracts\Repository as AbstractRepository;
use DM\Repositories\V1\InterfaceMethods\SubSubcategoryRepositoryInterface;
use DM\Models\V1\SubSubcategory\SubSubcategory;
use DM\Models\V1\Ad\Ad;

class SubSubcategoryRepository extends AbstractRepository implements SubSubcategoryRepositoryInterface
{
    protected $model;

    /**
     * SubSubcategoryRepository constructor
     *
     * @param SubSubcategory $subsubcategory
     */
    public function __construct(SubSubcategory $subsubcategory)
    {
        $this->model = $subsubcategory;
    }

    /**
     * Get All subcategories list
     *
     * @return \DM\Models\V1\Subcategory\Subcategory
     */
    public function getSubSubcategories($subcategory)
    {
        $subInfo = $this->model
          ->whereSubcategoryId($subcategory)
              ->with('listingCategoriesSubsubcategories')->get();
        if (!$subInfo) {
            abort(404);
        }
        return $subInfo;
    }

    /**
     * Get Subcategory Id By Slug
     * @param $slug
     *
     * @return \DM\Models\V1\Subcategory\Subcategory
     */
    public function findIdBySlug($slug)
    {
        $slugId = $this->model->whereSlug($slug)->first();
        if (!$slugId) {
            abort(404);
        }
        return $slugId;
    }

    /**
     * Get count Ads owned by subsubcategory
     *
     * @param $category
     *
     * @return int Total
     */
    public function getAdTitle($subsubcategory)
    {
        $subsubcategoriesCollection = $this->model
           ->whereId($subsubcategory)->first();
        $subsubid = $subsubcategoriesCollection->id;
        $array = ['subsubid' => $subsubid,
             'category_title' => $subsubcategoriesCollection->name,
             'category_ad_count' => Ad::whereHas('adListingCategory', function ($q) use ($subsubid) {
                 if ($subsubid) {
                     $q->where('subsub_id', $subsubid);
                 }
             })->count(),
             'category_ad_private_count' => Ad::leftJoin('users', 'users.id', '=', 'ad.user_id')
                ->whereHas('adListingCategory', function ($q) use ($subsubid) {
                    if ($subsubid) {
                        $q->where('subsub_id', $subsubid);
                    }
                })->where('users.user_type', 'private')->count(),
             'category_ad_company_count' => Ad::leftJoin('users', 'users.id', '=', 'ad.user_id')
                ->whereHas('adListingCategory', function ($q) use ($subsubid) {
                    if ($subsubid) {
                        $q->where('subsub_id', $subsubid);
                    }
                })->where('users.user_type', 'company')->count()
          ];

        return $array;
    }
}
