var elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

var bower_path = "./vendor/bower_components";
var paths = {
  'bootstrap': bower_path + "/bootstrap-sass/assets",
  'bourbon': bower_path + "/bourbon/app/assets",
  'fontawesome': bower_path + "/font-awesome",
}

elixir(function (mix) {
  mix.browserify('app.js')
  mix.sass('app.scss', 'public/css', null, {
    includePaths: [
      paths.bootstrap + '/stylesheets',
      paths.bourbon + '/stylesheets',
      paths.fontawesome + '/scss',
    ]
  })
  .copy(bower_path + '/fontawesome/scss', 'resources/assets/sass/vendor/fontawesome')
  .copy(bower_path + '/fontawesome/fonts/', 'public/build/fonts/')
  .copy(bower_path + '/owl.carousel/dist/owl.carousel.min.js', 'public/js/owl.carousel.min.js')
  .copy(bower_path + '/sumo-select/jquery.sumoselect.min.js', 'public/js/jquery.sumoselect.min.js')
  .version([
    'css/app.css',
    'js/app.js',
    'js/controllers.js'
     ]
  )
  mix.scripts([
      'controllers/marks.js',
    ], 'public/js/controllers.js')
    // mix.scripts([
    //   'models/UserModel.js',
    // ], 'public/js/models.js')
})
